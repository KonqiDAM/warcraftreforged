﻿using System;

class MineralTower : Building
{
    public static int cost = 200;

    public MineralTower(int x, int y,
                        string img = "data/buildings/mineralTower.png")
                        : base(x, y, img, 48,48)
    {
        health = 500;
    }
}

