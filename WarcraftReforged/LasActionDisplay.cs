﻿using System;
using System.Collections.Generic;

public class LasActionDisplay
{
    Font font18;
    List<Action> actions;

    public LasActionDisplay()
    {
        actions = new List<Action>();
        font18 = new Font("data/fonts/joystix.ttf", 18);
    }

    public void AddAction(string text)
    {
        actions.Add(new Action(text));
    }

    public void Draw()
    {
        for (int i = 0; i < actions.Count; i++)
        {
            SdlHardware.WriteHiddenText(actions[i].GetText(), (short)(300)
                                        , (short)(10+(i*10)),
                                   255, 255, 255, font18);
        }
    }


    public void DeleteOld()
    {
       /* foreach (Action a in actions)
            if (a.GetCreationTime() - DateTime.Now > 5)
                actions.Remove(a);*/
    }

}

