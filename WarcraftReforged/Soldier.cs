﻿using System;

class Soldier : Unit
{
    public static int cost = 50;

    public Soldier(int x, int y, int extraDamage = 0, int extraHealth = 0,
                   string img = null) : base(x, y, 48, 48, 40, 30+extraDamage )
    {
        baseHealth = 200;
        xSpeed = 6;
        ySpeed = 6;
        maxHealth = baseHealth+extraHealth;
        health = maxHealth;

        if (img == null)
        {
            LoadSequence(DOWN,
                         new string[] {
            "data/units/soldier/d1.png",
            "data/units/soldier/d2.png",
            "data/units/soldier/d3.png"}
                        );
            LoadSequence(UP,
                         new string[] {
            "data/units/soldier/u1.png",
            "data/units/soldier/u2.png",
            "data/units/soldier/u3.png"}
                        );
            LoadSequence(LEFT,
                         new string[] {
            "data/units/soldier/l1.png",
            "data/units/soldier/l2.png",
            "data/units/soldier/l3.png"}
                        );
            LoadSequence(RIGHT,
                         new string[] {
            "data/units/soldier/r1.png",
            "data/units/soldier/r2.png",
            "data/units/soldier/r3.png"}
                        );
        }
        else
        {
            LoadImage(img);
        }
    }
}

