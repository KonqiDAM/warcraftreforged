﻿using System;

class PauseMenu
{
    Image background;
    TextButton continueButton;
    TextButton save;
    TextButton load;
    TextButton help;
    TextButton exit;

    bool continueGame = true;

    HelpScreen h;

    public PauseMenu()
    {
        background = new Image("data/menus/pauseMenu.png");
        continueButton = new TextButton("Continue", 200, 200, 130, 30, 20);
        save = new TextButton("Save", 200, 250, 75, 30, 20);
        load = new TextButton("Load", 200, 300, 75, 30, 20);
        help = new TextButton("Tutorial/Help", 200, 350, 210, 30, 20);
        exit = new TextButton("Return to main menu", 200, 400, 345, 30, 20);

        h = HelpScreen.GetInstance();
    }

    public bool Run()
    {
        do
        {
            SdlHardware.DrawHiddenImage(background, 0, 0);
            continueButton.Draw();
            save.Draw();
            load.Draw();
            help.Draw();
            exit.Draw();
            SdlHardware.ShowHiddenScreen();
            if ((continueButton.CheckSelected(
                SdlHardware.GetMouseX(),
                SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
                || SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
            {
                while (SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
                {
                    ;
                }
                return true;
            }
            else if (exit.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                return false;
            }
            else if (save.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                //Todo
                return true;
            }
            else if (load.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                //Todo
                return true;
            }
            else if (help.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                h.Run();
            }
            SdlHardware.Pause(60);
        } while (continueGame);
        return true;
    }
}

