﻿using System;

public class EndScreen
{
    Image background;
    TextButton exit;
    Font font25;
    static EndScreen end;

    private  EndScreen()
    {
        background = new Image("data/menus/gameOver.png");
        exit = new TextButton("Exit", 200, 600, 100, 30, 20);
        font25 = new Font("data/fonts/joystix.ttf", 25);
    }

    public static EndScreen GetInstance()
    {
        if (end == null)
        {
            end = new EndScreen();
        }
        return end;
    }

    public void Run()
    {
        do
        {
            SdlHardware.DrawHiddenImage(background, 0, 0);
            exit.Draw();
            SdlHardware.ShowHiddenScreen();

            if (exit.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                return;
            }
            SdlHardware.Pause(50);

        } while (true);
    }
}

