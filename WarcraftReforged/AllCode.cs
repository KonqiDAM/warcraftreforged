﻿using System;

public class Action
{
    protected string text;
    protected DateTime creationTime;
    public Action(string text)
    {
        this.text = text;
        creationTime = DateTime.Now;
    }

    public string GetText() { return text; }
    public DateTime GetCreationTime() { return creationTime; }
}

﻿using System;
using System.Collections.Generic;

class AIEnemy
{


    void RecruitNewUnit(List<Unit> units)
    {
        if( rnd.Next() % 2 == 0)
        {
            
        }


    }

    public void Run(ref List<Building> buildings, ref List<Unit> units, List<Unit> eu, List<Building> eb)
    {
        if(CanCreateNewUnit())
        {
            RecruitNewUnit(units);
        }
    }
}

﻿using System;

class Barrack : Building
{
    public static int cost = 400;

    ImageButton soldier, horseman, pyro;

    public Barrack(int x, int y, string img = "data/buildings/barracks.png") 
        : base(x, y, img, 96, 96)
    {
        soldier = new ImageButton("data/ui/infantery.png",
                              WarcraftReforged.WIDTH - 50,
                              WarcraftReforged.HEIGTH - 50, 48, 48);
        horseman = new ImageButton("data/ui/horseman.png",
                               WarcraftReforged.WIDTH - 100,
                              WarcraftReforged.HEIGTH - 50, 48, 48);
        pyro = new ImageButton("data/ui/archer.png",
                             WarcraftReforged.WIDTH - 150,
                              WarcraftReforged.HEIGTH - 50, 48, 48);
        health = 1600;
    }

    override public void DrawOptions()
    {
        soldier.Draw();
        horseman.Draw();
        pyro.Draw();
    }

    public override int CheckSelectedOptions(int x, int y)
    {
        if (soldier.CheckSelected(x, y))
        {
            return 1;
        }
        if (horseman.CheckSelected(x, y))
        {
            return 3;
        }
        if (pyro.CheckSelected(x, y))
        {
            return 2;
        }
        return 0;
    }
}

﻿using System;

abstract class Building : Sprite
{
    protected bool selected;
    protected Font font18;

    public bool IsSelected() { return selected; }

    public Building(int x, int y, string img, int width, int height)
        : base(width, height)
    {
        LoadImage(img);
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        font18 = new Font("data/fonts/joystix.ttf", 18);
        visionRange = 400;
    }

    public int GetVisionRange() { return visionRange; }

    public bool CheckSelected(int x, int y)
    {
        if (Game.debuging)
        {
            SdlHardware.WriteHiddenText("A", (short)(this.x), 
                        (short)(this.y), 255, 255, 255, font18);

            SdlHardware.WriteHiddenText("e", (short)(this.x + width),
                        (short)(this.y + height), 255, 255, 255, font18);
            SdlHardware.ShowHiddenScreen();
        }

        if (this.x <= x
            && this.x + width >= x
            && this.y <= y
            && this.y + height >= y
        )
            selected = true;
        else
            selected = false;
        return selected;
    }

    virtual public int CheckSelectedOptions(int x, int y)
    {
        return 0;
    }

    virtual public void DrawOptions()
    {
        
    }

    public void ReceiveAttack(int damage)
    {
        health -= damage;
        if (health <= 0)
            visible = false;
    }
}

﻿using System;
using System.IO;

public class BuildingsHelpScrren
{
    Image background;
    Image transparency;
    TextButton exit;
    Font font25;
    static BuildingsHelpScrren cr;
    string[] text;

    private BuildingsHelpScrren()
    {
        background = new Image("data/menus/creditsMenu.jpg");
        transparency = new Image("data/misc/black.png");
        exit = new TextButton("Return", 200, 680, 100, 30, 20);
        font25 = new Font("data/fonts/joystix.ttf", 25);
        text = File.ReadAllLines("data/texts/buildings.txt");
    }

    public static BuildingsHelpScrren GetInstance()
    {
        if (cr == null)
        {
            cr = new BuildingsHelpScrren();
        }
        return cr;
    }

    public void Run()
    {

        do
        {
            SdlHardware.Pause(100);
            SdlHardware.DrawHiddenImage(background, 0, 0);
            SdlHardware.DrawHiddenImage(transparency, 0, 0);
            exit.Draw();


            for (int i = 0; i < text.Length; i++)
            {
                string line = text[i];
                SdlHardware.WriteHiddenText(
                    line
                    , (short)(50), (short)(50+i*30), 255, 255, 255, font25);
            }

            SdlHardware.ShowHiddenScreen();

            if ((exit.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
               || SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
            {
                while (SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
                {
                    ;
                }
                return;
            }
        } while (true);
    }
}

﻿using System;

public class Credits
{
    Image background;
    Image transparency;
    TextButton exit;
    Font font25;
    Font font20;
    static Credits cr;

    private  Credits()
    {
        background = new Image("data/menus/creditsMenu.jpg");
        transparency = new Image("data/misc/black.png");
        exit = new TextButton("Return", 200, 600, 100, 30, 20);
        font25 = new Font("data/fonts/joystix.ttf", 25);
        font20 = new Font("data/fonts/joystix.ttf", 20);
    }

    public static Credits GetInstance()
    {
        if( cr == null)
        {
            cr = new Credits();
        }
        return cr;
    }

    public void Run()
    {
        
        do
        {
            SdlHardware.DrawHiddenImage(background, 0, 0);
            SdlHardware.DrawHiddenImage(transparency, 0, 0);
            exit.Draw();

            SdlHardware.WriteHiddenText(
                "WarcraftRefoged created by Ivan Lazcano Sindin"
                , 250, 50, 255, 255, 255, font25);

            SdlHardware.WriteHiddenText(
                "Creaded with TAO_SDL and C# using mono implementation"
                , 300, 200, 255, 255, 255, font20);
            
            SdlHardware.WriteHiddenText(
                "Developed under GNU/Linux with MonoDevelop"
                , 300, 230, 255, 255, 255, font20);

            SdlHardware.ShowHiddenScreen();

            if((exit.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1)) 
               || SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
            {
                return;
            }
            SdlHardware.Pause(50);

        } while (true);
       
    }
}

﻿using System;

public class EndScreen
{
    Image background;
    TextButton exit;
    Font font25;
    static EndScreen end;

    private  EndScreen()
    {
        background = new Image("data/menus/gameOver.png");
        exit = new TextButton("Exit", 200, 600, 100, 30, 20);
        font25 = new Font("data/fonts/joystix.ttf", 25);
    }

    public static EndScreen GetInstance()
    {
        if (end == null)
        {
            end = new EndScreen();
        }
        return end;
    }

    public void Run()
    {
        do
        {
            SdlHardware.DrawHiddenImage(background, 0, 0);
            exit.Draw();
            SdlHardware.ShowHiddenScreen();

            if (exit.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                return;
            }
            SdlHardware.Pause(50);

        } while (true);
    }
}

﻿/**
 * Font.cs - To hide SDL TTF font handling
 * 
 * Changes:
 * 0.01, 24-jul-2013: Initial version, based on SdlMuncher 0.02
 */

using System;
using Tao.Sdl;

class Font
{
    private IntPtr internalPointer;

    public Font(string fileName, short sizePoints)
    {
        Load(fileName, sizePoints);
    }

    public void Load(string fileName, short sizePoints)
    {
        internalPointer = SdlTtf.TTF_OpenFont(fileName, sizePoints);
        if (internalPointer == IntPtr.Zero)
            SdlHardware.FatalError("Font not found: " + fileName);
    }

    public IntPtr GetPointer()
    {
        return internalPointer;
    }
}
﻿/**
 * Game.cs - WarcraftReforged
 * Ivan Lazcano Sindin
 *
 */

using System;
using Tao.Sdl;
using System.Collections.Generic;
using System.IO;

struct Upgrades 
{
    public int extraHealth;
    public int extraDamage;
}

class Game
{

    StreamWriter debugFile;
    ProgressBar energyBar;
    Image uiBackground;
    Image combatImg;
    DateTime lastCreatedUnit;

    Sprite actualConstructing;
    Sprite actualRecuiting;
    Map defaultMap;
    PauseMenu pause;
    EndScreen gameOverScreen;

    Font font18;

    List<Unit> units;
    List<Unit> enemyUnits;
    List<Building> buildings;
    List<Building> enemyBuildings;
    ImageButton[] menuOptions;
    ImageButton pauseButton;
    Upgrades[] unitsUpgrades;
    Random rnd;
    Unit aux;

    int enemyMinTimeCreationNewUnit;
    int xCore, yCore, yEnemyCore, xEnemyCore;
    public static short xoffSet;
    public static short yoffSet;
    short scrollSpeed;
    int actualEnergy;
    int maxEnergy;
    int actionOfUnit;

    short uiHeiht;

    bool finished;

    bool constructing;
    bool drawDefaultOptions;
    bool recuiting;
    bool movingUnit;
    int typeConstructing;
    int typeUnitToRecuit;
    public static bool debuging;

    bool debugingToFile;

    void InitEnemyInfo()
    {
        lastCreatedUnit = DateTime.Now;
        xCore = 20;
        yCore = 20;
        yEnemyCore = 1200;
        xEnemyCore = 3000;
        rnd = new Random();
    }

    void InitUnitsOfPlayer()
    {
        actionOfUnit = 0;
        units = new List<Unit>();
        units.Add(new Pyro(60, 60));
        units.Add(new Soldier(120, 60));
        units.Add(new Pyro(60, 120));
        units.Add(new Horseman(120, 120));
        enemyUnits = new List<Unit>();
        enemyUnits.Add(new Pyro(1200, 1200,0,0,"data/units/soldier.png"));
        unitsUpgrades = new Upgrades[3];

        //1 soldier
        //2 pyto
        //3 horseman

        unitsUpgrades[0].extraDamage = 0;
        unitsUpgrades[0].extraHealth = 0;
        unitsUpgrades[1].extraDamage = 0;
        unitsUpgrades[1].extraHealth = 0;
        unitsUpgrades[2].extraDamage = 0;
        unitsUpgrades[2].extraHealth = 0;
    }



    void Init()
    {
        pause = new PauseMenu();
        debuging = false;
        constructing = false;
        drawDefaultOptions = true;
        finished = false;
        recuiting = false;
        debugingToFile = false;
        finished = false;
        movingUnit = false;
        InitUnitsOfPlayer();
        try
        {
            defaultMap = new Map(1);
            defaultMap.GetCoreCordinates(
                out xCore, out  yCore, out xEnemyCore, out yEnemyCore);
            if(debuging)
                Console.WriteLine(xCore+" "+yCore+
                                  "\n"+xEnemyCore+" "+yEnemyCore);

        }
        catch (Exception)
        {
            Console.WriteLine("Exited with error on map!");
            Environment.Exit(-1);
        }

        uiBackground = new Image("data/ui/brown.jpg");
        combatImg = new Image("data/ui/combat.png");
        pauseButton = new ImageButton(
            "data/ui/menu.png", WarcraftReforged.WIDTH - 107, 0, 107, 24);
        GenerateMenuOption();

        font18 = new Font("data/fonts/joystix.ttf", 18);

        enemyMinTimeCreationNewUnit = 10;
        actualEnergy = 360;
        maxEnergy = 10000;
        energyBar = new ProgressBar(maxEnergy, 1110);
        rnd = new Random();
        gameOverScreen = EndScreen.GetInstance();

        buildings = new List<Building>
        {
            new Barrack(500, 500),
            new Barrack(2000, 1000)
        };
        enemyBuildings = new List<Building>
        {
            new Barrack(1000, 1200, "data/buildings/b2.png")
        };
        if (debugingToFile)
        {
            debugFile = new StreamWriter("debugFile.txt");
        }
        if (debuging)
            actualEnergy = 999999;

    }

    void GenerateMenuOption()
    {
        //In constructing menu
        //1 - barrack
        //2 - MineralTower
        //3 - Laboratory

        menuOptions = new ImageButton[3];
        menuOptions[0] = new ImageButton("data/ui/barracks.png",
                                         WarcraftReforged.WIDTH - 50,
                                         WarcraftReforged.HEIGTH - 50,
                                         48, 48);
        menuOptions[2] = new ImageButton("data/ui/laboratory.png",
                                         WarcraftReforged.WIDTH - 100,
                                         WarcraftReforged.HEIGTH - 50,
                                         48, 48);
        menuOptions[1] = new ImageButton("data/ui/mineral.png",
                                         WarcraftReforged.WIDTH - 150,
                                         WarcraftReforged.HEIGTH - 50,
                                         48, 48);

        uiHeiht = 120;
        xoffSet = 0;
        yoffSet = 0;
        scrollSpeed = 40;

    }

    void DrawUnitInformation(Unit u)
    {
        SdlHardware.WriteHiddenText("Type:" + u,
                                    (short)(500 - xoffSet)
                                    , (short)(655 - yoffSet),
                                    255, 255, 255, font18);
        SdlHardware.WriteHiddenText("Health:" + u.GetHealth(),
                                    (short)(500 - xoffSet),
                                    (short)(675 - yoffSet),
                                    255, 255, 255, font18);
        SdlHardware.WriteHiddenText("Level:" + u.GetLevel(),
                                    (short)(500 - xoffSet),
                                    (short)(695 - yoffSet),
                                    255, 255, 255, font18);
        SdlHardware.WriteHiddenText("Damage:" + u.GetAttackDamage(),
                                    (short)(500 - xoffSet), 
                                    (short)(715 - yoffSet),
                                    255, 255, 255, font18);
        SdlHardware.WriteHiddenText("Speed:" + u.GetSpeedX(),
                                        (short)(500 - xoffSet), 
                                        (short)(735 - yoffSet),
                                        255, 255, 255, font18);
    }

    void DrawBuildingInfomration(Building b)
    {
            if(b.IsSelected())
            {
                SdlHardware.WriteHiddenText("Type:" + b,
                                        (short)(500 - xoffSet)
                                        , (short)(655 - yoffSet),
                                        255, 255, 255, font18);
                SdlHardware.WriteHiddenText("Health:" + b.GetHealth(),
                                            (short)(500 - xoffSet),
                                            (short)(675 - yoffSet),
                                            255, 255, 255, font18);
                SdlHardware.WriteHiddenText("Range:" + b.GetVisionRange(),
                                        (short)(500 - xoffSet),
                                        (short)(695 - yoffSet),
                                        255, 255, 255, font18);

            }
    }

    bool DrawUnitsBuildingsOptions()
    {
        drawDefaultOptions = true;
        foreach (Building b in buildings)
        {
            if (b.IsSelected())
            {
                b.DrawOptions();
                DrawBuildingInfomration(b);
                drawDefaultOptions = false;
            }
        }

        foreach (Unit u in units)
        {
            if (u.IsSelected())
            {
                u.DrawOptions();
                DrawUnitInformation(u);
                drawDefaultOptions = false;
            }
        }
        return drawDefaultOptions;
    }

    void DrawUIBar()
    {
        SdlHardware.DrawHiddenImage(uiBackground, -xoffSet,
                                    650 - yoffSet, 1366, uiHeiht);

        SdlHardware.WriteHiddenText("Energy:" + actualEnergy,
                                    (short)(10 - xoffSet)
                                    , (short)(659 - yoffSet),
                                    255, 255, 255, font18);
    }

    void DrawUi()
    {
        pauseButton.Draw();

        DrawUIBar();
        drawDefaultOptions = DrawUnitsBuildingsOptions();

        //TODO fix energyBar
        //energyBar.Draw((short)(200-xoffSet), (short)(660-yoffSet),
        //               actualEnergy);

        SdlHardware.WriteHiddenText("Total units: " + units.Count,
                                    (short)(10 - xoffSet),
                                    (short)(745 - yoffSet),
                                    255, 255, 255, font18);
        //if (constructing)
            //drawDefaultOptions = false;

        if(drawDefaultOptions)
        {
            for (int i = 0; i < menuOptions.Length; i++)
            {
                menuOptions[i].Draw();
            }
        }
    }

    void DrawCombatImg(Unit u)
    {
        SdlHardware.DrawHiddenImage(
            combatImg, u.GetX() 
            + u.GetWidth()/2 - 12, u.GetY() - 26);
    }


    void CheckVisibilityOfUnits()
    {
        foreach (Unit e in enemyUnits)
            e.SetIsVisibleByEnemy(false);

        foreach (Unit u in units)
        {
            foreach (Unit e in enemyUnits)
                if (u.InVisionRange(e))
                    e.SetIsVisibleByEnemy(true);
        }

        foreach(Building b in buildings)
            foreach (Unit e in enemyUnits)
                if (b.InVisionRange(e))
                    e.SetIsVisibleByEnemy(true);

    }

    void DrawUnits()
    {
        CheckVisibilityOfUnits();
        foreach (Unit s in units)
        {
            s.DrawOnHiddenScreen();
            if (s.IsSelected())
                s.DrawOptions();
            if (s.InCombat())
            {
                DrawCombatImg(s);
            }
        }

        foreach (Unit e in enemyUnits)
        {
            if(e.IsVisibleByEnemy())
                e.DrawOnHiddenScreen();
            if (e.InCombat())
                DrawCombatImg(e);
        }

        if (debuging)
            foreach (Unit ss in units)
                if (ss.IsSelected())
                    SdlHardware.WriteHiddenText(
                    "x:" + ss.GetX() +
                    ", y: " + ss.GetY(),
                    (short)-xoffSet,
                    (short)-yoffSet,
                    125, 125, 125, font18);
    }

    void DrawBuildings()
    {
        foreach(Building b in buildings)
            b.DrawOnHiddenScreen();
        
        foreach (Building b in enemyBuildings)
            b.DrawOnHiddenScreen();
    }

    void UpdateScreen()
    {
        SdlHardware.ClearScreen();
        defaultMap.Draw(units, buildings);
        DrawUnits();
        DrawBuildings();
        DrawUi();

        if(constructing)
        {
            actualConstructing.MoveTo(SdlHardware.GetMouseX()
                              -actualConstructing.GetWidth()/2-xoffSet,
                              SdlHardware.GetMouseY()
                              -actualConstructing.GetHeight()/2-yoffSet);
            actualConstructing.DrawOnHiddenScreen();
        }

        if(recuiting)
        {
            actualRecuiting.MoveTo(
                SdlHardware.GetMouseX()
                - actualRecuiting.GetWidth() / 2 - xoffSet,
                SdlHardware.GetMouseY()
                - actualRecuiting.GetHeight() / 2 - yoffSet);
            actualRecuiting.DrawOnHiddenScreen();
        }

        SdlHardware.ShowHiddenScreen();
    }

    void CheckScroll()
    {
        if (SdlHardware.KeyPressed(SdlHardware.KEY_RIGHT)
            || SdlHardware.GetMouseX() > 1361)
        {
            if (xoffSet - scrollSpeed - WarcraftReforged.WIDTH
                >= -defaultMap.WIDTH)
            {
                SdlHardware.ScrollHorizontally((short)(-scrollSpeed));
                xoffSet -= scrollSpeed;
            }
        }

        if (SdlHardware.KeyPressed(SdlHardware.KEY_LEFT)
            || SdlHardware.GetMouseX() < 5)
        {
            if (xoffSet + scrollSpeed <= 0)
            {
                SdlHardware.ScrollHorizontally((short)(scrollSpeed));
                xoffSet += scrollSpeed;
            }
        }

        if (SdlHardware.KeyPressed(SdlHardware.KEY_UP)
            || SdlHardware.GetMouseY() < 5) 
        {
            if (yoffSet + scrollSpeed <= 0)
            {
                SdlHardware.ScrollVertically((short)(scrollSpeed));
                yoffSet += scrollSpeed;
            }
        }

        if (SdlHardware.KeyPressed(SdlHardware.KEY_DOWN)
            || SdlHardware.GetMouseY() > 762)
        {
            if (yoffSet - scrollSpeed - WarcraftReforged.HEIGTH + 
                uiHeiht  >= -defaultMap.HEIGHT)
            {
                SdlHardware.ScrollVertically((short)(-scrollSpeed));
                yoffSet -= scrollSpeed;
            }
        }
    }

    void CheckConstructionType()
    {
        switch (typeConstructing)
        {
            case 1:
                if (actualEnergy >= Barrack.cost)
                {
                    actualEnergy -= Barrack.cost;
                    buildings.Add(new Barrack(
                        actualConstructing.GetX(),
                        actualConstructing.GetY()));
                }
                break;
            case 2:
                if (actualEnergy >= MineralTower.cost)
                {
                    actualEnergy -= MineralTower.cost;
                    buildings.Add(new MineralTower(
                        actualConstructing.GetX(),
                        actualConstructing.GetY()));
                }
                break;
            case 3:
                if (actualEnergy >= Lab.cost)
                {
                    actualEnergy -= Lab.cost;
                    buildings.Add(new Lab(
                        actualConstructing.GetX(),
                        actualConstructing.GetY()));
                }
                break;
        }
    }

    void MineralTowerCons()
    {
        int tmpX = 0, tmpY = 0;
        if (defaultMap.IsMineral(actualConstructing,
                                 ref tmpX, ref tmpY))
        {
            constructing = true;
            actualConstructing.MoveTo(tmpX, tmpY);
            //we want to put the mineralTower 
            //exactly on the original sprite point
        }
        else
            constructing = false;
    }

    void CheckCollisionVisionConstructing()
    {
        foreach (Unit s in units)
        {
            if (s.CollisionsWith(actualConstructing) 
                && !s.InVisionRange(actualConstructing))
                constructing = false;
        }

        foreach (Unit e in enemyUnits)
        {
            if (e.CollisionsWith(actualConstructing))
                constructing = false;
        }

        if (defaultMap.ColisionWithMap(actualConstructing))
        {
            constructing = false;
        }
    }

    bool CheckInVision(Sprite s)
    {
        bool inVision = false;
        foreach(Unit u in units)
        {
            if (u.InVisionRange(s))
                inVision = true;
        }
        foreach(Building b in buildings)
            if (b.InVisionRange(s))
                inVision = true;

        return inVision;
    }

    void CheckConstruction()
    {
        if (typeConstructing == 2)
        {
            MineralTowerCons();
        }
        else
        {
            CheckCollisionVisionConstructing();
        }

        foreach (Building b in buildings)
            if (b.CollisionsWith(actualConstructing))
                constructing = false;

        foreach (Building b in enemyBuildings)
            if (b.CollisionsWith(actualConstructing))
                constructing = false;
        
        if (constructing && CheckInVision(actualConstructing))
        {
            CheckConstructionType();
            constructing = false;
        }
    }

    void CheckRecuitingUnitType()
    {

        //1 soldier
        //2 pyto
        //3 horseman
        switch (typeUnitToRecuit)
        {
            case 1:
                if (actualEnergy >= Soldier.cost)
                {
                    actualEnergy -= Soldier.cost;
                    units.Add(new Soldier(
                        actualRecuiting.GetX(),
                        actualRecuiting.GetY(),
                        unitsUpgrades[0].extraDamage,
                        unitsUpgrades[0].extraHealth));
                }
                break;
            case 2:
                if (actualEnergy >= Pyro.cost)
                {
                    actualEnergy -= Pyro.cost;
                    units.Add(new Pyro(
                        actualRecuiting.GetX(),
                        actualRecuiting.GetY(),
                        unitsUpgrades[1].extraDamage,
                        unitsUpgrades[1].extraHealth));
                }
                break;
            case 3:
                if (actualEnergy >= Horseman.cost)
                {
                    actualEnergy -= Horseman.cost;
                    units.Add(new Horseman(
                        actualRecuiting.GetX(),
                        actualRecuiting.GetY(),
                        unitsUpgrades[2].extraDamage,
                        unitsUpgrades[2].extraHealth));
                }
                break;
        }
        recuiting = false;
    }

    void CheckRecruitCollision()
    {
        foreach (Unit s in units)
            if (s.CollisionsWith(actualRecuiting))
                recuiting = false;

        foreach (Unit e in enemyUnits)
            if(e.CollisionsWith(actualRecuiting))
                recuiting = false;

        if (defaultMap.ColisionWithMap(actualRecuiting))
            recuiting = false;

        foreach (Building b in buildings)
            if (b.CollisionsWith(actualRecuiting))
                recuiting = false;
        
        foreach(Building b in enemyBuildings)
            if (b.CollisionsWith(actualRecuiting))
                recuiting = false;

        if (recuiting && CheckInVision(actualRecuiting))
        {
            CheckRecuitingUnitType();
        }
    }

    void MoveUnit()
    {
        foreach (Unit u in units)
        {
            if (u.IsSelected())
            {
                switch(actionOfUnit)
                {
                    case 1:
                        u.WalkTo(SdlHardware.GetMouseX() - xoffSet,
                             SdlHardware.GetMouseY() - yoffSet);
                        break;
                    case 2:
                        u.WalkTo(SdlHardware.GetMouseX() - xoffSet,
                               SdlHardware.GetMouseY() - yoffSet, false);
                        break;
                    case 3:
                        u.SetPatrolling(
                            SdlHardware.GetMouseX() - xoffSet,
                            SdlHardware.GetMouseY() - yoffSet);
                        break;
                }
            }
        }
                // 1 - move
                // 2 - atack move
                // 3 - patrol
    }

    void LaunchPause()
    {
        while (SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
        {
            ;
        }
        SdlHardware.ResetScroll();
        if (!pause.Run())
        {
            finished = true;
            yoffSet = 0;
            xoffSet = 0;
        }
        SdlHardware.ScrollTo(xoffSet, yoffSet);
    }

    void CheckLeftClick()
    {
        if(pauseButton.CheckSelected(SdlHardware.GetMouseX(),
                                     SdlHardware.GetMouseY()))
        {
            LaunchPause();
        }
        if (movingUnit)
            MoveUnit();
        foreach (Building b in buildings)
            b.CheckSelected(SdlHardware.GetMouseX() - xoffSet,
                        SdlHardware.GetMouseY() - yoffSet);
        
        if (constructing)
            CheckConstruction();

        if(recuiting)
            CheckRecruitCollision();

        foreach (Unit s in units)
            if (s.CheckSelected(SdlHardware.GetMouseX() - xoffSet,
                                SdlHardware.GetMouseY() - yoffSet))
                s.MoveTo(s.GetX(), s.GetY());
        
        movingUnit = false;
    }

    int SelectedUnit()
    {
        //The game was planed to have the ability to select multiple units
        //but not now :)
        for (int i = 0; i < units.Count; i++)
        {
            if (units[i].IsSelected())
                return i;
        }
        return 0;
    }

    void EscPressed()
    {
        if (!constructing && !recuiting)
        {
            LaunchPause();
        }
        else
        {
            constructing = false;
            recuiting = false;
        }
    }

    void CheckKeys()
    {
        int selectedUnit = SelectedUnit();

        if (SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
            EscPressed();
        
        if (SdlHardware.KeyPressed(SdlHardware.KEY_B))
            Construct(1);
        
        if (SdlHardware.KeyPressed(SdlHardware.KEY_M))
            Construct(2);

        if (SdlHardware.KeyPressed(SdlHardware.KEY_L))
            Construct(3);
       
        if(SdlHardware.KeyPressed(SdlHardware.KEY_P))
            units[selectedUnit].SetPatrolling(
                SdlHardware.GetMouseX() - xoffSet,
                SdlHardware.GetMouseY() - yoffSet);
            
        if (SdlHardware.KeyPressed(SdlHardware.KEY_A))
            units[selectedUnit].WalkTo(SdlHardware.GetMouseX() - xoffSet,
                             SdlHardware.GetMouseY() - yoffSet, false);

        if (SdlHardware.KeyPressed(SdlHardware.KEY_S))
            units[selectedUnit].SetStop();
    }

    void CheckUpgrades(int returned)
    {
        switch (returned)
        {
            case 0:
                break;
            case 1:
                unitsUpgrades[0].extraHealth += 25;
                unitsUpgrades[1].extraHealth += 15;
                unitsUpgrades[2].extraHealth += 20;
                break;
        }
    }

    void CheckRecuitment(int returned)
    {
        switch (returned)
        {
            case 0:
                break;
            case 1:
                recuiting = true;
                typeUnitToRecuit = 1;
                actualRecuiting = new Sprite(
                    "data/units/soldier/d1.png", 48, 48);
                if(debuging)
                    Console.WriteLine("soldier");
                break;
            case 2:
                recuiting = true;
                typeUnitToRecuit = 2;
                actualRecuiting = new Sprite(
                    "data/units/pyro/d1.png", 48, 48);
                break;
            case 3:
                recuiting = true;
                typeUnitToRecuit = 3;
                actualRecuiting = new Sprite(
                    "data/units/horseman/d1.png", 48, 48);
                break;
        }
    }

    void CheckConstructingMenu()
    {
        for (int i = 0; i < menuOptions.Length; i++)
        {
            menuOptions[i].CheckSelected(
            SdlHardware.GetMouseX(),
            SdlHardware.GetMouseY());
            if (menuOptions[i].IsSelected())
            {
                //1 - barrack
                //2 - MineralTower
                //3 - Laboratory
                switch (i)
                {
                    case 0:
                        Construct(1);
                     break;
                    case 1:
                        Construct(2);
                        break;
                    case 2:
                        Construct(3);
                        break;
                }
            }
        }
    }

    void CheckBuildingOptions()
    {
        foreach (Building b in buildings)
        {
            if (b.IsSelected())
            {
                int returned =
                        b.CheckSelectedOptions(
                        SdlHardware.GetMouseX(),
                        SdlHardware.GetMouseY());
                if (b.GetType() == typeof(Lab))
                {
                    CheckUpgrades(returned);
                }
                else if (b.GetType() == typeof(Barrack))
                {
                    CheckRecuitment(returned);
                }
            }
        }
    }

    void CheckUnitOptions()
    {
        foreach(Unit u in units)
        {
            if(u.IsSelected())
            {
                actionOfUnit =
                        u.CheckSelectedOptions(
                        SdlHardware.GetMouseX(),
                        SdlHardware.GetMouseY());
                if(actionOfUnit != 0)
                {
                    movingUnit = true;
                }
            }
        }
    }

    void CheckUiClick()
    {
        if (!drawDefaultOptions)
        {
            CheckBuildingOptions();
            CheckUnitOptions();
        }
        else
        {
            CheckConstructingMenu();
        }
    }

    void CheckUserInput()
    {
        CheckScroll();
        CheckKeys();

        if (SdlHardware.MouseClicked(1))
        {
            if (WarcraftReforged.HEIGTH -
                SdlHardware.GetMouseY() < uiHeiht)
            {
                CheckUiClick();
            }
            else
                CheckLeftClick();
        }

        if (SdlHardware.MouseClicked(2))
            foreach (Unit s in units)
                if (s.IsSelected())
                    s.WalkTo(SdlHardware.GetMouseX() - xoffSet, 
                             SdlHardware.GetMouseY() - yoffSet);
        
    }

    bool IsOutOfMap(Sprite u)
    {
        if(u.GetX() < 0 || u.GetY() < 0 ||
           u.GetX() > defaultMap.WIDTH ||
           u.GetY() > defaultMap.HEIGHT)
        {
            return false;
        }
        return true;
    }

    bool CollidesWithObjet(Unit i)
    {
        foreach (Building b in enemyBuildings)
            if (b.CollisionsWith(i))
                return true;
        
        foreach (Building b in buildings)
            if (b.CollisionsWith(i))
                return true;

        for (int j = 0; j < units.Count; j++)
        {
            if (units[j] == i)//we dont want to check colision with him self
                continue;
            if (Math.Abs(i.GetX() - units[j].GetX()) < 130 &&
                i.CollisionsWith(units[j]))
            return true;
        }

        foreach (Unit e in enemyUnits)
            if (i.CollisionsWith(e) && e != i)
                return true;

        if (defaultMap.ColisionWithMap(i))
            return true;

        return !IsOutOfMap(i);
    }

    void MoveUnits(List<Unit> soldiers)
    {
        //we try to move all soldiers, if they colide with another sprite
        //they cant move to that position
        int oldX, oldY;

        for (int i = 0; i < soldiers.Count; i++)
        {
            oldX = soldiers[i].GetX();
            oldY = soldiers[i].GetY();

            soldiers[i].MoveX();

            if (CollidesWithObjet(soldiers[i]))
                soldiers[i].MoveTo(oldX, oldY);

            soldiers[i].MoveY();

            if (CollidesWithObjet(soldiers[i]))
                soldiers[i].MoveTo(oldX, oldY);

        }
    }

    void Atack(List<Unit> units, List<Unit> enemyUnits,
               List<Building> enemyBuildings)
    {
        foreach (Unit u in units)
        {
            foreach (Building b in enemyBuildings)
                if (u.CanAttack(b))
                {
                    u.Attack(b);
                    if (b.GetHealth() < 0)
                        u.LevelUP();
                }

            foreach (Unit e in enemyUnits)
                if (u.CanAttack(e))
                {
                    u.Attack(e);
                    if (e.GetHealth() < 0)
                        u.LevelUP();
                }
        }
    }

    void Combat()
    {
        Atack(enemyUnits, units, buildings);
        Atack(units, enemyUnits, enemyBuildings);
    }

    void RemoveSprite(List<Unit> s)
    {
        int countDeleted = 0;
        for (int i = 0; i < s.Count - countDeleted; i++)
        {
            if (s[i].GetHealth() <= 0)
            {
                s.Remove(s[i]);
                countDeleted++;
            }
        }
    }

    void RemoveBuildings(List<Building> s)
    {
        int countDeleted = 0;
        for (int i = 0; i < s.Count - countDeleted; i++)
        {
            if (s[i].GetHealth() <= 0)
            {
                s.Remove(s[i]);
                countDeleted++;
            }
        }
    }

    void RemoveDeadSoldier()
    {
        RemoveSprite(units);
        RemoveSprite(enemyUnits);
    }

    void RemoveDestructedBuildings()
    {
        RemoveBuildings(buildings);
        RemoveBuildings(enemyBuildings);
    }

    void UpdateWorld()
    {
        foreach (Building b in buildings)
        {
            if (b.GetType() == typeof(MineralTower))
            {
                actualEnergy += 1;
                if (actualEnergy > maxEnergy)
                    actualEnergy = maxEnergy;
            }
        }
        RemoveDeadSoldier();
        RemoveDestructedBuildings();
    }

    void Construct(int type)
    {
        //1 - barrack
        //2 - MineralTower
        //3 - Laboratory
        typeConstructing = type;
        switch(type)
        {
            case 1:
                actualConstructing = new Sprite("data/buildings/" +
                                                "barracks.png", 96, 96);
                constructing = true;
                break;
            case 2:
                actualConstructing = new Sprite("data/buildings/" +
                                                "mineralTower.png", 48, 48);
                constructing = true;
                break;
            case 3:
                actualConstructing = new Sprite("data/buildings/" +
                                                "laboratory.png", 96, 96);
                constructing = true;
                break;
        }
    }

    void DebugToFile()
    {
        debugFile.WriteLine("Energy: " + actualEnergy);
    }

    void RefreshCoolDownUnits()
    {
        foreach (Unit u in units)
            u.RefreshAttackColldown();
        foreach (Unit e in enemyUnits)
            e.RefreshAttackColldown();
    }

    bool CanCreateNewUnit()
    {
        if ((DateTime.Now - lastCreatedUnit).TotalSeconds
            > enemyMinTimeCreationNewUnit)
        {
            lastCreatedUnit = DateTime.Now;
            return true;
        }
        return false;
    }

    void RecruitNewUnit()
    {
        if (rnd.Next() % 2 == 0)
        {
            if (rnd.Next() % 2 == 0)
            {
                aux = new Soldier(xEnemyCore, yEnemyCore,
                                       0, 0, "data/units/s.png");
            }
            else
            {
                aux = new Pyro(xEnemyCore, yEnemyCore,
                                    0, 0, "data/units/soldier.png");
            }
            if(debuging)
                Console.WriteLine("Try to create a " + aux);
            bool done = false;
            for (int i = 0; i < 15 && !done ; i++)
            {
                aux.MoveTo(rnd.Next(xEnemyCore - 500, xEnemyCore + 500),
                            rnd.Next(yEnemyCore - 300, yEnemyCore + 300));
                if(!CollidesWithObjet(aux))
                {
                    done = true;
                    if(debuging)
                        Console.WriteLine("Done");
                    enemyUnits.Add(aux);
                }
            }
        }
    }

    void ExecuteEnemyAI()
    {
        if (CanCreateNewUnit())
        {
            RecruitNewUnit();
        }
        if(enemyUnits.Count > 15)
        {
            if(debuging)
                Console.WriteLine("Atacking core!");
            foreach(Unit u in enemyUnits)
            {
                u.WalkTo(xCore,yCore);
            }
        }
    }

    void GoToEnemy(List<Unit> units, List<Unit> enemyUnits)
    {
        foreach (Unit u in units)
            foreach (Unit e in enemyUnits)
            {
                if (u.InVisionRange(e) && !u.IsStopped())
                    u.WalkTo(e.GetX() - e.GetWidth() / 2,
                             e.GetY() - e.GetHeight() / 2,
                             false);
                
                if (e.InVisionRange(u))
                    e.WalkTo(u.GetX() - u.GetWidth() / 2,
                         u.GetY() - u.GetHeight() / 2,
                             false);
            }
    }

    void HealUnits(List<Unit> units)
    {
        foreach(Unit u in units)
        {
            u.HealIfOutOfCombat();
        }
    }

    void AImovements()
    {
        RefreshCoolDownUnits();
        ExecuteEnemyAI();
        GoToEnemy(units, enemyUnits);
        GoToEnemy(enemyUnits, units);
        MoveUnits(units);
        MoveUnits(enemyUnits);
        Combat();
        HealUnits(units);
        HealUnits(enemyUnits);
    }

    void PauseUntilNextFrame()
    {
        SdlHardware.Pause(30);
    }

    void CheckWinCondition()
    {
        if (buildings.Count == 0)
        {
            finished = true;
            yoffSet = 0;
            xoffSet = 0;
            SdlHardware.ResetScroll();
            gameOverScreen.Run();
        }
        else if(enemyBuildings.Count == 0)
        {
            finished = true;
            yoffSet = 0;
            xoffSet = 0;
            SdlHardware.ResetScroll();
            gameOverScreen.Run();

            //TODO Launch win screen

        }
    }

    public void Run()
    {
        Init();

        do
        {
            AImovements();
            CheckUserInput();
            UpdateWorld();
            UpdateScreen();
            PauseUntilNextFrame();
            if (debugingToFile)
                DebugToFile();
            CheckWinCondition();
        }
        while (!finished);
        if(debugingToFile)
        {
            debugFile.Close();
        }
    }
}﻿using System;
using System.IO;
public class HelpScreen
{
    Image background;
    Image transparency;
    TextButton exit;
    TextButton unitsHelp;
    TextButton buildingHelp;

    UnitsHelpScreen uHelp;
    BuildingsHelpScrren bHelp;

    Font font25;
    Font font20;
    static HelpScreen hlp;

    private HelpScreen()
    {
        background = new Image("data/menus/helpMenu.png");
        transparency = new Image("data/misc/black.png");
        exit = new TextButton("Return", 200, 680, 100, 30, 20);
        unitsHelp = new TextButton("Unit help", 200, 600, 145, 30, 20);
        buildingHelp = new TextButton("Building help", 200, 640, 215, 30, 20);
        font25 = new Font("data/fonts/joystix.ttf", 25);
        font20 = new Font("data/fonts/joystix.ttf", 20);
        uHelp = UnitsHelpScreen.GetInstance();
        bHelp = BuildingsHelpScrren.GetInstance();
    }

    public static HelpScreen GetInstance()
    {
        if(hlp == null)
        {
            hlp = new HelpScreen();
        }
        return hlp;
    }

    public void Run(string file = "data/texts/help.txt")
    {
        SdlHardware.Pause(100);
        do
        {
            SdlHardware.DrawHiddenImage(background, 0, 0);
            SdlHardware.DrawHiddenImage(transparency, 0, 0);
            exit.Draw();
            unitsHelp.Draw();
            buildingHelp.Draw();
            if(!File.Exists(file))
            {
                Console.WriteLine("Help menu file does not exist!");
                Environment.Exit(-1);
            }

            try
            {
                StreamReader helpText = new StreamReader(file);
                short posX = 150;
                short posY = 100;
                string line = helpText.ReadLine();
                while(line != null)
                {
                    if (line.Contains("//"))
                    {
                        line = line.Substring(0, line.IndexOf("//"));
                    }
                    if(line.Contains(":"))
                    {
                        SdlHardware.WriteHiddenText(
                            line
                            , (short)(posX-50), posY, 255, 255, 255, font25);
                    }
                    else
                    {
                        SdlHardware.WriteHiddenText(
                            line
                            , posX, posY, 255, 255, 255, font25);
                    }
                    posY += 30;
                    line = helpText.ReadLine();
                }

                helpText.Close();
            }
            catch (PathTooLongException)
            {
                Console.WriteLine("Path to long!");
                Environment.Exit(-1);
            }
            catch (IOException)
            {
                Console.WriteLine("Help menu text file error!");
                Environment.Exit(-1);
            }
            catch (Exception)
            {
                Console.WriteLine("Error on file reading!");
                Environment.Exit(-1);
            }


            SdlHardware.ShowHiddenScreen();


                if (( exit.CheckSelected(SdlHardware.GetMouseX(),
                                        SdlHardware.GetMouseY())
                     && SdlHardware.MouseClicked(1))
                    || SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
                {
                    while (SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
                    {
                        ;
                    }
                    return;
                }
                else if(unitsHelp.CheckSelected(SdlHardware.GetMouseX(),
                                                SdlHardware.GetMouseY()) 
                        && SdlHardware.MouseClicked(1))
                {
                    uHelp.Run();
                }
                else if(buildingHelp.CheckSelected(SdlHardware.GetMouseX(),
                                                   SdlHardware.GetMouseY())
                            && SdlHardware.MouseClicked(1))
                {
                    bHelp.Run();
                }
        } while (true);
    }
}

﻿﻿using System;

class Horseman : Unit
{
    public static int cost = 70;

    public Horseman(int x, int y, int extraDamage = 0, int extraHealth = 0,
                   string img = null) : base(x, y, 48, 48, 30, 25+extraDamage)
    {
        baseHealth = 125;
        maxHealth = baseHealth + extraHealth;
        xSpeed = ySpeed = 12;
        health = maxHealth;

        if (img == null)
        {
            LoadSequence(DOWN,
                         new string[] {
            "data/units/horseman/d1.png",
            "data/units/horseman/d2.png"}
                        );
            LoadSequence(UP,
                         new string[] {
            "data/units/horseman/u1.png",
            "data/units/horseman/u2.png"}
                        );
            LoadSequence(LEFT,
                         new string[] {
            "data/units/horseman/l1.png",
            "data/units/horseman/l2.png"}
                        );
            LoadSequence(RIGHT,
                         new string[] {
            "data/units/horseman/r1.png",
            "data/units/horseman/r2.png"}
                        );
        }
        else
            LoadImage(img);
    }
}

﻿using System;

class ImageButton : Sprite
{
    protected bool selected;
    public ImageButton(String img, int x, int y, int width, int height) : base(width, height)
    {
        LoadImage(img);
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        selected = false;
    }

    public bool IsSelected() { return selected; }

    public bool CheckSelected(int x, int y)
    {
        if (
           this.x <= x
            && this.x + width >= x
            && this.y <= y
            && this.y + height >= y)
        {
            selected = true;
        }
        else
        {
            selected = false;
        }
        return selected;
    }

    public void Draw()
    {
        SdlHardware.DrawHiddenImage(image, x-Game.xoffSet, y-Game.yoffSet);
    }
}

﻿/**
 * Image.cs - To hide SDL image handling
 * 
 * Changes:
 * 0.01, 24-jul-2013: Initial version, based on SdlMuncher 0.02
 */



using System;
using Tao.Sdl;

class Image
{
    private IntPtr internalPointer;

    public Image(string fileName)  // Constructor
    {
        Load(fileName);
    }

    public void Load(string fileName)
    {
        internalPointer = SdlImage.IMG_Load(fileName);
        if (internalPointer == IntPtr.Zero)
            SdlHardware.FatalError("Image not found: " + fileName);
    }


    public IntPtr GetPointer()
    {
        return internalPointer;
    }
}
﻿using System;

class Lab : Building
{
    public static int cost = 100;
    protected ImageButton medic;

    public Lab(int x, int y,
               string img = "data/buildings/laboratory.png") 
        : base(x, y, img, 96, 96)
    {
        medic = new ImageButton("data/ui/medic.png",
                              WarcraftReforged.WIDTH - 50,
                              WarcraftReforged.HEIGTH - 50,
                              48, 48);
        health = 1100;
    }

    public override void DrawOptions()
    {
        medic.Draw();
    }

    public override int CheckSelectedOptions(int x, int y)
    {
        if (medic.CheckSelected(x, y))
        {
            if(Game.debuging)
                Console.WriteLine("upgrade!");
            return 1;
        }
        return 0;
    }
}

﻿using System;
using System.Collections.Generic;

public class LasActionDisplay
{
    Font font18;
    List<Action> actions;

    public LasActionDisplay()
    {
        actions = new List<Action>();
        font18 = new Font("data/fonts/joystix.ttf", 18);
    }

    public void AddAction(string text)
    {
        actions.Add(new Action(text));
    }

    public void Draw()
    {
        for (int i = 0; i < actions.Count; i++)
        {
            SdlHardware.WriteHiddenText(actions[i].GetText(), (short)(300)
                                        , (short)(10+(i*10)),
                                   255, 255, 255, font18);
        }
    }


    public void DeleteOld()
    {
       /* foreach (Action a in actions)
            if (a.GetCreationTime() - DateTime.Now > 5)
                actions.Remove(a);*/
    }

}

﻿using System;

public class MainMenu
{
    Image background;
    TextButton singlePLayer;
    TextButton multiplayer;
    TextButton credits;
    TextButton help;
    TextButton exit;

    bool continueGame = true;

    Game g;
    Credits c;
    HelpScreen h;

    public MainMenu()
    {
        background = new Image("data/menus/mainMenu.jpg");
        singlePLayer = new TextButton("Single Player", 200, 200, 205, 30, 20);
        multiplayer = new TextButton("Multiplayer", 200, 250, 175, 30, 20);
        credits = new TextButton("Credits", 200, 300, 120, 30, 20);
        help = new TextButton("Tutorial/Help", 200, 350, 210, 30, 20);
        exit = new TextButton("Exit game", 200, 400, 145, 30, 20);
        g = new Game();
        c = Credits.GetInstance();
        h = HelpScreen.GetInstance();
    }

    public void Run()
    {
        do
        {
            SdlHardware.DrawHiddenImage(background, 0, 0);
            singlePLayer.Draw();
            multiplayer.Draw();
            credits.Draw();
            help.Draw();
            exit.Draw();
            SdlHardware.ShowHiddenScreen();
            if (singlePLayer.CheckSelected(SdlHardware.GetMouseX(),
                                          SdlHardware.GetMouseY())
               && SdlHardware.MouseClicked(1))
            {
                g.Run();
            }
            else if (exit.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                continueGame = false;
            }
            else if (credits.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                c.Run();
            }
            else if (help.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                h.Run();
            }
            SdlHardware.Pause(50);
        } while (continueGame);
    }
}﻿using System.IO;
using System;
using System.Collections.Generic;

class Map
{
    Sprite fog;
    protected SortedList<int, string> mapName;
    protected string file;
    protected Image grass, water, mineral,
    ground1, ground2, mineralTower, nexus, enemyCore,
    transparent;
    bool tileInVision;

    protected string[] map;
    protected int tileWidth;
    public int WIDTH { get; }
    public int HEIGHT { get; }

    public Map(int fileName)
    {
        mapName = new SortedList<int, string>();
        mapName.Add(1, "data/maps/1.map");
        mapName.Add(2, "data/maps/2.map");
        file = mapName[fileName];
        //bool mapReaded = true;
        transparent = new Image("data/misc/transparent.png");//s
        nexus = new Image("data/buildings/Nexus.png");//W
        grass = new Image("data/ground/grass1.png");//G
        water = new Image("data/ground/water1.png");//G
        mineral = new Image("data/buildings/mineral.png");//M
        ground1 = new Image("data/ground/ground1.png");//H
        ground2 = new Image("data/ground/ground2.png");//J
        mineralTower = new Image("data/buildings/mineralTower.png");//W
        enemyCore = new Image("data/buildings/E.png");
        tileWidth = 48;
        fog = new Sprite("data/ground/blackTransparent.png",
                         tileWidth, tileWidth);


        if(!File.Exists(file))
        {
            Console.WriteLine("Map file does not exist!");
            throw new Exception("File error!");
        }
        else
        {
            map = File.ReadAllLines(file);
        }

        HEIGHT = tileWidth * map.Length;
        WIDTH = tileWidth * map[0].Length;
    }

    public void GetCoreCordinates( out int x, out int y,
                                  out int xe, out int ye)
    {
        x = y = xe = ye = 0;
        for (int i = 0; i < map.Length; i++)
        {
            for (int j = 0; j < map[i].Length; j++)
            {
                switch (map[i][j])
                {
                    case 'S':
                        x = i * tileWidth;
                        y = j * tileWidth;
                        break;
                    case 'E':
                        xe = i * tileWidth;
                        ye = j * tileWidth;
                        break;
                }
            }
        }
    }


    public void DrawFog(List<Unit> units, 
                        List<Building> buildings, 
                        int x, int y)
    {
        tileInVision = false;

        fog.MoveTo(x,y);
        for (int i = 0; i < units.Count && !tileInVision; i++)
            if (units[i].InVisionRange(fog))
                tileInVision = true;
            
        for (int i = 0; i < buildings.Count && !tileInVision; i++)
            if (buildings[i].InVisionRange(fog))
                tileInVision = true;

        if (!tileInVision)
            fog.DrawOnHiddenScreen();
    }

    public void Draw(List <Unit> units, List<Building> buildings)
    {
        int posX, posY;
        Image aux;
        for (int y = 0; y < map.Length; y++)
        {
            for (int x = 0; x < map[y].Length; x++)
            {
                posX = x * tileWidth;
                posY = y * tileWidth;

                switch (map[y][x])
                {
                    case 'W':
                        aux = water;
                        break;
                    case 'S':
                        aux = nexus;
                        break;
                    case 'e':
                    case 's':
                        aux = transparent;
                        break;
                    case 'M':
                        aux = mineral;
                        break;
                    case 'E':
                        aux = enemyCore;
                        break;
                    default:
                        aux = grass;
                        break;
                }
                SdlHardware.DrawHiddenImage(aux, posX, posY);
                DrawFog(units, buildings, posX, posY);
            }
        }
    }

    public bool IsMineral(Sprite tower, ref int x, ref int y)
    {
        if(map[(tower.GetY()+tower.GetHeight()/2) /
               tileWidth][(tower.GetX()+tower.GetWidth()/2) /
                          tileWidth] == 'M')
        {
            x = (tower.GetX()+ tower.GetHeight() / 2) / tileWidth * tileWidth;
            y = (tower.GetY()+ tower.GetWidth() / 2) / tileWidth * tileWidth;
            //we want to return the position of the sprite 
            return true;
        }
        return false;
    }

    public bool ColisionWithMap(Sprite unit, char tileChar = ' ')
    {
        int x2tile, y2tile,x1tile, y1tile;
        for (int i = 0; i < map[0].Length; i++)
        {
            for (int j = 0; j < map.Length; j++)
            {
                char tile = map[j][i];
                if(tile != tileChar)
                {
                    x1tile = i * tileWidth;
                    y1tile = j * tileWidth;
                    x2tile = x1tile + tileWidth;
                    y2tile = y1tile + tileWidth;

                    if((x1tile < unit.GetX()+unit.GetWidth()) &&
                       (x2tile > unit.GetX()) &&
                       (y1tile < unit.GetY()+unit.GetHeight()) &&
                       (y2tile > unit.GetY()))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

﻿using System;

class MineralTower : Building
{
    public static int cost = 200;

    public MineralTower(int x, int y,
                        string img = "data/buildings/mineralTower.png")
                        : base(x, y, img, 48,48)
    {
        health = 500;
    }
}

﻿using System;

class PauseMenu
{
    Image background;
    TextButton continueButton;
    TextButton save;
    TextButton load;
    TextButton help;
    TextButton exit;

    bool continueGame = true;

    HelpScreen h;

    public PauseMenu()
    {
        background = new Image("data/menus/pauseMenu.png");
        continueButton = new TextButton("Continue", 200, 200, 130, 30, 20);
        save = new TextButton("Save", 200, 250, 75, 30, 20);
        load = new TextButton("Load", 200, 300, 75, 30, 20);
        help = new TextButton("Tutorial/Help", 200, 350, 210, 30, 20);
        exit = new TextButton("Return to main menu", 200, 400, 345, 30, 20);

        h = HelpScreen.GetInstance();
    }

    public bool Run()
    {
        do
        {
            SdlHardware.DrawHiddenImage(background, 0, 0);
            continueButton.Draw();
            save.Draw();
            load.Draw();
            help.Draw();
            exit.Draw();
            SdlHardware.ShowHiddenScreen();
            if ((continueButton.CheckSelected(
                SdlHardware.GetMouseX(),
                SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
                || SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
            {
                while (SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
                {
                    ;
                }
                return true;
            }
            else if (exit.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                return false;
            }
            else if (save.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                //Todo
                return true;
            }
            else if (load.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                //Todo
                return true;
            }
            else if (help.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                h.Run();
            }
            SdlHardware.Pause(60);
        } while (continueGame);
        return true;
    }
}

﻿using System;

public class ProgressBar
{
    protected int max;
    protected int width;
    protected string line;

    Image black;

    Font font15;

    public ProgressBar(int max, int width)
    {
        this.max = max;
        this.width = width;
        font15 = new Font("data/fonts/joystix.ttf", 15);
        black = new Image("data/misc/black.png");
    }

    public void Draw(short x, short y, int actual)
    {
        line = "";
        line += new string('*',  actual/width);
        line += new string('-', (max - actual)/width);

        SdlHardware.DrawHiddenImage(black, x - 5, y - 5, (short)width, 25);
        SdlHardware.WriteHiddenText(line, x, y, 255, 255, 255, font15);
    }
}﻿using System;

class Pyro : Unit
{
    public static int cost = 30;

    public Pyro(int x, int y, int extraDamage = 0, int extraHealth = 0,
                string img = null) : base(x, y, 33, 48, 30, 35+extraDamage)
    {
        baseHealth = 175;
        maxHealth = baseHealth + extraHealth;
        xSpeed = 4;
        ySpeed = 4;
        health = maxHealth;

        if (img == null)
        {
            LoadSequence(DOWN,
                         new string[] {
            "data/units/pyro/d1.png",
            "data/units/pyro/d2.png",
            "data/units/pyro/d3.png"}
                        );
            LoadSequence(UP,
                         new string[] {
            "data/units/pyro/u1.png",
            "data/units/pyro/u2.png",
            "data/units/pyro/u3.png"}
                        );
            LoadSequence(LEFT,
                         new string[] {
            "data/units/pyro/l1.png",
            "data/units/pyro/l2.png",
            "data/units/pyro/l3.png"}
                        );
            LoadSequence(RIGHT,
                         new string[] {
            "data/units/pyro/r1.png",
            "data/units/pyro/r2.png",
            "data/units/pyro/r3.png"}
                        );
        }
        else
        {
            LoadImage(img);
        }
    }
}

﻿/**
 * SdlHardware.cs - Hides SDL library
 * 
 * Nacho Cabanes, 2013
 * 
 * Changes:
 * 0.01, 24-jul-2013: Initial version, based on SdlMuncher 0.14
 */

using System.IO;
using System.Threading;
using Tao.Sdl;
using System;

class SdlHardware
{
    static IntPtr hiddenScreen;
    static short width, height;

    static short startX, startY; // For Scroll

    static bool isThereJoystick;
    static IntPtr joystick;

    static int mouseClickLapse;
    static int lastMouseClick;


    public static void Init(short w, short h, int colors, bool fullScreen)
    {
        width = w;
        height = h;

        int flags = Sdl.SDL_HWSURFACE | Sdl.SDL_DOUBLEBUF | Sdl.SDL_ANYFORMAT;
        if (fullScreen)
            flags |= Sdl.SDL_FULLSCREEN;
        Sdl.SDL_Init(Sdl.SDL_INIT_EVERYTHING);
        hiddenScreen = Sdl.SDL_SetVideoMode(
            width,
            height,
            colors,
            flags);

        Sdl.SDL_Rect rect2 =
            new Sdl.SDL_Rect(0, 0, (short)width, (short)height);
        Sdl.SDL_SetClipRect(hiddenScreen, ref rect2);

        SdlTtf.TTF_Init();

        // Joystick initialization
        isThereJoystick = true;
        if (Sdl.SDL_NumJoysticks() < 1)
            isThereJoystick = false;

        if (isThereJoystick)
        {
            joystick = Sdl.SDL_JoystickOpen(0);
            if (joystick == IntPtr.Zero)
                isThereJoystick = false;
        }

        // Time lapse between two consecutive mouse clicks,
        // so that they are not too near
        mouseClickLapse = 20;
        lastMouseClick = Sdl.SDL_GetTicks();
    }

    public static void ClearScreen()
    {
        Sdl.SDL_Rect origin = new Sdl.SDL_Rect(0, 0, width, height);
        Sdl.SDL_FillRect(hiddenScreen, ref origin, 0);
    }

    public static void DrawHiddenImage(Image image, int x, int y)
    {
        drawHiddenImage(image.GetPointer(), x + startX, y + startY, 0, 0,
                        width,height);
    }

    public static void DrawHiddenImage(Image image, int x, int y, 
                                       short width, short height)
    {
        drawHiddenImage(image.GetPointer(), x + startX, y + startY, 0, 0,
                        width, height);
    }


    public static void DrawHiddenImage(Image image, int x, int y, 
                                       short iniX, short iniY, short 
                                       width, short height)
    {
        drawHiddenImage(image.GetPointer(), x + startX, y + startY,
                        iniX, iniY, width, height);
    }

    public static void ShowHiddenScreen()
    {
        Sdl.SDL_Flip(hiddenScreen);
    }

    public static bool KeyPressed(int c)
    {
        bool pressed = false;
        Sdl.SDL_PumpEvents();
        Sdl.SDL_Event myEvent;
        Sdl.SDL_PollEvent(out myEvent);
        int numkeys;
        byte[] keys = Tao.Sdl.Sdl.SDL_GetKeyState(out numkeys);
        if (keys[c] == 1)
            pressed = true;
        return pressed;
    }

    public static void Pause(int milisegundos)
    {
        Thread.Sleep(milisegundos);
    }

    public static int GetWidth()
    {
        return width;
    }

    public static int GetHeight()
    {
        return height;
    }

    public static void FatalError(string text)
    {
        StreamWriter sw = File.AppendText("errors.log");
        sw.WriteLine(text);
        sw.Close();
        Console.WriteLine(text);
        Environment.Exit(1);
    }

    public static void WriteHiddenText(string txt,
        short x, short y, byte r, byte g, byte b, Font f)
    {
        Sdl.SDL_Color color = new Sdl.SDL_Color(r, g, b);
        IntPtr textoComoImagen = SdlTtf.TTF_RenderText_Solid(
            f.GetPointer(), txt, color);
        if (textoComoImagen == IntPtr.Zero)
            Environment.Exit(5);

        Sdl.SDL_Rect origen = new Sdl.SDL_Rect(0, 0, width, height);
        Sdl.SDL_Rect dest = new Sdl.SDL_Rect(
            (short)(x + startX), (short)(y + startY),
            width, height);

        Sdl.SDL_BlitSurface(textoComoImagen, ref origen,
            hiddenScreen, ref dest);
    }

    // Scroll Methods

    public static void ResetScroll()
    {
        startX = startY = 0;
    }

    public static void ScrollTo(short newStartX, short newStartY)
    {
        startX = newStartX;
        startY = newStartY;
    }

    public static void ScrollHorizontally(short xDespl)
    {
        startX += xDespl;
    }

    public static void ScrollVertically(short yDespl)
    {
        startY += yDespl;
    }

    // Joystick methods

    /** JoystickPressed: returns TRUE if
        *  a certain button in the joystick/gamepad
        *  has been pressed
        */
    public static bool JoystickPressed(int boton)
    {
        if (!isThereJoystick)
            return false;

        if (Sdl.SDL_JoystickGetButton(joystick, boton) > 0)
            return true;
        else
            return false;
    }

    /** JoystickMoved: returns TRUE if
        *  the joystick/gamepad has been moved
        *  up to the limit in any direction
        *  Then, int returns the corresponding
        *  X (1=right, -1=left)
        *  and Y (1=down, -1=up)
        */
    public static bool JoystickMoved(out int posX, out int posY)
    {
        posX = 0; posY = 0;
        if (!isThereJoystick)
            return false;

        posX = Sdl.SDL_JoystickGetAxis(joystick, 0);  // Leo valores (hasta 32768)
        posY = Sdl.SDL_JoystickGetAxis(joystick, 1);
        // Normalizo valores
        if (posX == -32768) posX = -1;  // Normalizo, a -1, +1 o 0
        else if (posX == 32767) posX = 1;
        else posX = 0;
        if (posY == -32768) posY = -1;
        else if (posY == 32767) posY = 1;
        else posY = 0;

        if ((posX != 0) || (posY != 0))
            return true;
        else
            return false;
    }


    /** JoystickMovedRight: returns TRUE if
        *  the joystick/gamepad has been moved
        *  completely to the right
        */
    public static bool JoystickMovedRight()
    {
        if (!isThereJoystick)
            return false;

        int posX = 0, posY = 0;
        if (JoystickMoved(out posX, out posY) && (posX == 1))
            return true;
        else
            return false;
    }

    /** JoystickMovedLeft: returns TRUE if
        *  the joystick/gamepad has been moved
        *  completely to the left
        */
    public static bool JoystickMovedLeft()
    {
        if (!isThereJoystick)
            return false;

        int posX = 0, posY = 0;
        if (JoystickMoved(out posX, out posY) && (posX == -1))
            return true;
        else
            return false;
    }


    /** JoystickMovedUp: returns TRUE if
        *  the joystick/gamepad has been moved
        *  completely upwards
        */
    public static bool JoystickMovedUp()
    {
        if (!isThereJoystick)
            return false;

        int posX = 0, posY = 0;
        if (JoystickMoved(out posX, out posY) && (posY == -1))
            return true;
        else
            return false;
    }


    /** JoystickMovedDown: returns TRUE if
        *  the joystick/gamepad has been moved
        *  completely downwards
        */
    public static bool JoystickMovedDown()
    {
        if (!isThereJoystick)
            return false;

        int posX = 0, posY = 0;
        if (JoystickMoved(out posX, out posY) && (posY == 1))
            return true;
        else
            return false;
    }


    /** GetMouseX: returns the current X
        *  coordinate of the mouse position
        */
    public static int GetMouseX()
    {
        int posX = 0, posY = 0;
        Sdl.SDL_PumpEvents();
        Sdl.SDL_GetMouseState(out posX, out posY);
        return posX;
    }


    /** GetMouseY: returns the current Y
        *  coordinate of the mouse position
        */
    public static int GetMouseY()
    {
        int posX = 0, posY = 0;
        Sdl.SDL_PumpEvents();
        Sdl.SDL_GetMouseState(out posX, out posY);
        return posY;
    }


    /** MouseClicked: return TRUE if
        *  the (left) mouse button has been clicked
        */
    public static bool MouseClicked(byte button)
    {
        int posX = 0, posY = 0;

        Sdl.SDL_PumpEvents();

        // To avoid two consecutive clicks
        int now = Sdl.SDL_GetTicks();
        if (now - lastMouseClick < mouseClickLapse)
            return false;
        
        // Ahora miro si realmente hay pulsación
        if ((Sdl.SDL_GetMouseState(out posX, out posY) & Sdl.SDL_BUTTON(button)) == button)
        {
            lastMouseClick = now;
            return true;
        }
        else
            return false;
    }


    // Private (auxiliar) methods


    private static void drawHiddenImage(IntPtr image, int x, int y, short iniX, short iniY, short width, short height)
    {
        Sdl.SDL_Rect origin = new Sdl.SDL_Rect(iniX, iniY, width, height);
        Sdl.SDL_Rect dest = new Sdl.SDL_Rect((short)x, (short)y,
                width, height);
        Sdl.SDL_BlitSurface(image, ref origin, hiddenScreen, ref dest);
    }


    // Alternate key definitions

    public static int KEY_ESC = Sdl.SDLK_ESCAPE;
    public static int KEY_SPC = Sdl.SDLK_SPACE;
    public static int KEY_A = Sdl.SDLK_a;
    public static int KEY_B = Sdl.SDLK_b;
    public static int KEY_C = Sdl.SDLK_c;
    public static int KEY_D = Sdl.SDLK_d;
    public static int KEY_E = Sdl.SDLK_e;
    public static int KEY_F = Sdl.SDLK_f;
    public static int KEY_G = Sdl.SDLK_g;
    public static int KEY_H = Sdl.SDLK_h;
    public static int KEY_I = Sdl.SDLK_i;
    public static int KEY_J = Sdl.SDLK_j;
    public static int KEY_K = Sdl.SDLK_k;
    public static int KEY_L = Sdl.SDLK_l;
    public static int KEY_M = Sdl.SDLK_m;
    public static int KEY_N = Sdl.SDLK_n;
    public static int KEY_O = Sdl.SDLK_o;
    public static int KEY_P = Sdl.SDLK_p;
    public static int KEY_Q = Sdl.SDLK_q;
    public static int KEY_R = Sdl.SDLK_r;
    public static int KEY_S = Sdl.SDLK_s;
    public static int KEY_T = Sdl.SDLK_t;
    public static int KEY_U = Sdl.SDLK_u;
    public static int KEY_V = Sdl.SDLK_v;
    public static int KEY_W = Sdl.SDLK_w;
    public static int KEY_X = Sdl.SDLK_x;
    public static int KEY_Y = Sdl.SDLK_y;
    public static int KEY_Z = Sdl.SDLK_z;
    public static int KEY_1 = Sdl.SDLK_1;
    public static int KEY_2 = Sdl.SDLK_2;
    public static int KEY_3 = Sdl.SDLK_3;
    public static int KEY_4 = Sdl.SDLK_4;
    public static int KEY_5 = Sdl.SDLK_5;
    public static int KEY_6 = Sdl.SDLK_6;
    public static int KEY_7 = Sdl.SDLK_7;
    public static int KEY_8 = Sdl.SDLK_8;
    public static int KEY_9 = Sdl.SDLK_9;
    public static int KEY_0 = Sdl.SDLK_0;
    public static int KEY_UP = Sdl.SDLK_UP;
    public static int KEY_DOWN = Sdl.SDLK_DOWN;
    public static int KEY_RIGHT = Sdl.SDLK_RIGHT;
    public static int KEY_LEFT = Sdl.SDLK_LEFT;
    public static int KEY_RETURN = Sdl.SDLK_RETURN;

                                      
}
﻿using System;

class Soldier : Unit
{
    public static int cost = 50;

    public Soldier(int x, int y, int extraDamage = 0, int extraHealth = 0,
                   string img = null) : base(x, y, 48, 48, 40, 30+extraDamage )
    {
        baseHealth = 200;
        xSpeed = 6;
        ySpeed = 6;
        maxHealth = baseHealth+extraHealth;
        health = maxHealth;

        if (img == null)
        {
            LoadSequence(DOWN,
                         new string[] {
            "data/units/soldier/d1.png",
            "data/units/soldier/d2.png",
            "data/units/soldier/d3.png"}
                        );
            LoadSequence(UP,
                         new string[] {
            "data/units/soldier/u1.png",
            "data/units/soldier/u2.png",
            "data/units/soldier/u3.png"}
                        );
            LoadSequence(LEFT,
                         new string[] {
            "data/units/soldier/l1.png",
            "data/units/soldier/l2.png",
            "data/units/soldier/l3.png"}
                        );
            LoadSequence(RIGHT,
                         new string[] {
            "data/units/soldier/r1.png",
            "data/units/soldier/r2.png",
            "data/units/soldier/r3.png"}
                        );
        }
        else
        {
            LoadImage(img);
        }
    }
}

﻿


using System;
/**
* Sprite.cs - A basic graphic element to inherit from
* 
* Changes:
* 0.01, 24-jul-2013: Initial version, based on SdlMuncher 0.12
*/
class Sprite
{
    protected int x, y;
    protected int startX, startY;
    protected int width, height;
    protected int xSpeed, ySpeed;
    protected bool visible;
    protected Image image;
    protected Image[][] sequence;
    protected bool containsSequence;
    protected int currentFrame;
    protected int health;
    protected int visionRange;

    protected byte numDirections = 11;
    protected byte currentDirection;
    public const byte RIGHT = 0;
    public const byte LEFT = 1;
    public const byte DOWN = 2;
    public const byte UP = 3;
    public const byte DOWNRIGHT = 4;
    public const byte DOWNLEFT = 5;
    public const byte UPRIGHT = 6;
    public const byte UPLEFT = 7;
    public const byte APPEARING = 8;
    public const byte DISAPPEARING = 9;
    public const byte JUMPING = 9;

    public Sprite(int width, int height)
    {
        startX = -1;
        startY = -1;
        this.width = width;
        this.height = height;
        visible = true;
        sequence = new Image[numDirections][];
        currentDirection = RIGHT;
    }

    public int GetHealth() { return health; }

    public Sprite(string imageName,int width, int height)
        : this(width, height)
    {
        LoadImage(imageName);
    }

    public Sprite(string[] imageNames,int width, int height)
        : this(width, height)
    {
        LoadSequence(imageNames);
    }

    public void LoadImage(string name)
    {
        image = new Image(name);
        containsSequence = false;
    }

    public void LoadSequence(byte direction, string[] names)
    {
        int amountOfFrames = names.Length;
        sequence[direction] = new Image[amountOfFrames];
        for (int i = 0; i < amountOfFrames; i++)
            sequence[direction][i] = new Image(names[i]);
        containsSequence = true;
        currentFrame = 0;
    }

    public void LoadSequence(string[] names)
    {
        LoadSequence(RIGHT, names);
    }

    public int GetX()
    {
        return x;
    }

    public int GetY()
    {
        return y;
    }

    public int GetWidth()
    {
        return width;
    }

    public int GetHeight()
    {
        return height;
    }

    public int GetSpeedX()
    {
        return xSpeed;
    }

    public int GetSpeedY()
    {
        return ySpeed;
    }

    public bool IsVisible()
    {
        return visible;
    }

    public void MoveTo(int newX, int newY)
    {
        x = newX;
        y = newY;
        if (startX == -1)
        {
            startX = x;
            startY = y;
        }
    }

    public void SetSpeed(int newXSpeed, int newYSpeed)
    {
        xSpeed = newXSpeed;
        ySpeed = newYSpeed;
    }

    public void Show()
    {
        visible = true;
    }

    public void Hide()
    {
        visible = false;
    }

    public virtual void Move()
    {
        // To be redefined in subclasses
    }

    public void DrawOnHiddenScreen()
    {
        if (!visible)
            return;

        if (containsSequence)
            SdlHardware.DrawHiddenImage(
                sequence[currentDirection][currentFrame], x, y);
        else
            SdlHardware.DrawHiddenImage(image, x, y);
    }

    public void NextFrame()
    {
        currentFrame++;
        if (currentFrame >= sequence[currentDirection].Length)
            currentFrame = 0;
    }

    public void ChangeDirection(byte newDirection)
    {
        if (!containsSequence) return;
        if (currentDirection != newDirection)
        {
            currentDirection = newDirection;
            currentFrame = 0;
        }

    }

    public bool CollisionsWith(Sprite otherSprite)
    {
        return (visible && otherSprite.IsVisible() &&
            CollisionsWith(otherSprite.GetX(),
                otherSprite.GetY(),
                otherSprite.GetX() + otherSprite.GetWidth(),
                otherSprite.GetY() + otherSprite.GetHeight()));
    }

    public bool CollisionsWith(int xStart, int yStart, int xEnd, int yEnd)
    {
        if (visible &&
                (x < xEnd) &&
                (x + width > xStart) &&
                (y < yEnd) &&
                (y + height > yStart)
                )
            return true;
        return false;
    }

    public void Restart()
    {
        x = startX;
        y = startY;
    }

    public bool InVisionRange(Sprite s)
    {
        //Check from the center of the body;
        if (Math.Abs((s.GetX() - s.GetWidth() / 2) - (x - width / 2))
           <= visionRange &&
           Math.Abs((s.GetY() - s.GetHeight() / 2) - (y - height / 2))
           <= visionRange)
        {
            return true;
        }
        return false;
    }


}
﻿using System;


class TextButton : Sprite
{
    Font font18;
    protected string text;
    protected bool selected;
    protected byte red;
    protected byte blue;
    protected byte green;

    public TextButton(String text, int x, int y, int width, int height, 
                      int fontSize = 10) : base(width, height)
    {
        this.text = text;
        font18 = new Font("data/fonts/joystix.ttf", (short) fontSize);
        LoadImage("data/misc/black.png");
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        selected = false;
        red = blue = green;
    }

    public bool IsSelected() { return selected; }

    public bool CheckSelected(int x, int y)
    {
        if (
           this.x <= x
        && this.x + width >= x
        && this.y <= y
        && this.y + height >= y
    )
        {
            selected = true;
            red = 255;
            blue = green = 0;
        }
        else
        {
            selected = false;
            red = green = blue = 255;

        }
        return selected;
    }

    public void Draw()
    {
        SdlHardware.DrawHiddenImage(image, x, y, 0, 0,
                                    (short)width, (short)height);
        SdlHardware.WriteHiddenText(text, (short)(x + 5),
                                    (short)(y + 5), red, green,
                                    blue, font18);
    }

    public override void Move()
    {
        base.Move();
    } 
}

﻿using System;


abstract class Unit : Sprite
{
    Font font18;
    bool isPatrolling;
    bool stop;
    bool isCombating;
    bool isVisibleByEnemy;
    protected bool selected;
    protected int walkToX;
    protected int walkToY;
    protected int range, attackDamage, level;
    int xInitPatrol, yInitPatrol, yFinalPatrol, xFinalPatrol;
    protected ImageButton patrol;
    protected ImageButton move;
    protected ImageButton attackMove;
    protected ImageButton stopB;
    protected int attackCooldown;
    protected int lastAttack;
    protected int maxHealth;
    protected static int baseHealth;

    public Unit(int x, int y, int width, int height, int range,
                int attackDamage) : base(width, height)
    {
        walkToX = this.x = x;
        walkToY = this.y = y;
        font18 = new Font("data/fonts/joystix.ttf", 18);
        currentDirection = DOWN;
        visionRange = 350;
        this.range = range;
        this.attackDamage = attackDamage;
        level = 1;
        stop = false;
        attackCooldown = 15;
        lastAttack = 0;
        isCombating = false;

        patrol = new ImageButton("data/ui/patrol.png",
                                WarcraftReforged.WIDTH - 50,
                                WarcraftReforged.HEIGTH - 50,
                                48, 48);

        move = new ImageButton("data/ui/move.png",
                                WarcraftReforged.WIDTH - 100,
                                WarcraftReforged.HEIGTH - 50,
                                48, 48);

        attackMove = new ImageButton("data/ui/atackMove.png",
                                WarcraftReforged.WIDTH - 150,
                                WarcraftReforged.HEIGTH - 50,
                                48, 48);

        stopB = new ImageButton("data/ui/stop.png",
                                WarcraftReforged.WIDTH - 200,
                                WarcraftReforged.HEIGTH - 50,
                                48, 48);
    }


    public int GetLevel() { return level; }
    public int GetAttackDamage() { return attackDamage; }
    public int GetRange() { return range; }

    public bool IsVisibleByEnemy() { return isVisibleByEnemy; }
    public void SetIsVisibleByEnemy( bool visible ) 
    { 
        this.isVisibleByEnemy = visible;
    }
    public bool IsStopped() { return stop; }
    public bool InCombat() { return isCombating; }
    public bool IsSelected() { return selected; }

    public void SetStop() { this.stop = true; WalkTo(x,y); }

    public void SetPatrolling(int x, int y)
    {
        xInitPatrol = this.x;
        yInitPatrol = this.y;
        xFinalPatrol = x;
        yFinalPatrol = y;
        isPatrolling = true;
    }


    public bool CheckSelected(int x, int y)
    {
        if (Game.debuging)
        {
            SdlHardware.WriteHiddenText("A", (short)(this.x),
                        (short)(this.y), 255, 255, 255, font18);

            SdlHardware.WriteHiddenText("E", (short)(this.x + width),
                        (short)(this.y + height), 255, 255, 255, font18);
            SdlHardware.ShowHiddenScreen();
        }

        if (this.x <= x
            && this.x + width >= x
            && this.y <= y
            && this.y + height >= y
        )
            selected = true;
        else
            selected = false;
        return selected;
    }

    public void WalkTo(int x, int y, bool forced = true)
    {
        stop = false;
        //only move to enemy if terminated forced move by user
        if ((!forced && walkToX == this.x && walkToY == this.y) || forced)
        {
            walkToX = x;
            walkToY = y;
            isPatrolling = false;
        }
    }

    public void MoveX()
    {
        if (isPatrolling)
            Patrol();
        if (Math.Abs(x - walkToX) <= xSpeed)
            x = walkToX;
        else
        {
            if (x < walkToX)
                MoveRight();
            else
                MoveLeft();
        }
    }

    public void MoveY()
    {
        if (isPatrolling)
            Patrol();
        if (Math.Abs(y - walkToY) <= ySpeed)
            y = walkToY;
        else
        {
            if (y < walkToY)
                MoveDown();
            else
                MoveUp();
        }
    }

    protected void Patrol()
    {
        if (x == xInitPatrol && y == yInitPatrol)
        {
            walkToX = xFinalPatrol;
            walkToY = yFinalPatrol;
        }
        else if (x == xFinalPatrol && y == yFinalPatrol)
        {
            walkToX = xInitPatrol;
            walkToY = yInitPatrol;
        }
    }

    protected void MoveRight()
    {
        if (containsSequence)
        {
            ChangeDirection(RIGHT);
            NextFrame();
        }
        x += xSpeed;
    }

    protected void MoveLeft()
    {
        if (containsSequence)
        {
            ChangeDirection(LEFT);
            NextFrame();
        }
        x -= xSpeed;
    }

    protected void MoveUp()
    {
        if (containsSequence)
        {
            ChangeDirection(UP);
            NextFrame();
        }
        y -= ySpeed;
    }

    protected void MoveDown()
    {
        if (containsSequence)
        {
            ChangeDirection(DOWN);
            NextFrame();
        }
        y += ySpeed;
    }

    public void ReceiveAttack(int damage, int level)
    {
        int levelDiference = this.level - level;
        if (levelDiference > 5)
            return;
        else if (levelDiference < 5)
            health -= damage + levelDiference * 2;
        else
        {
            health -= new Random().Next(0, 10) < 8 ? damage : 0;
        }
    }

    public bool CanAttack(Sprite s)
    {
        if(s.CollisionsWith(x-range,y-range,x+range+width,y+range+height))
        {
            isCombating = true;
        }
        else
            isCombating = false;
        return isCombating;
    }

    public void Attack(Unit u)
    {
        if (lastAttack <= 0)
        {
            u.ReceiveAttack(attackDamage, level);
            lastAttack = attackCooldown;
        }
    }

    public void Attack(Building u)
    {
        if (lastAttack <= 0)
        {
            u.ReceiveAttack(attackDamage);
            lastAttack = attackCooldown;
        }
    }

    public void RefreshAttackColldown()
    {
        if (lastAttack > 0)
            lastAttack--;
    }

    public void HealIfOutOfCombat()
    {
        if (lastAttack <= 0 && health < maxHealth)
        {
            Console.WriteLine("Cura tt");
            Console.WriteLine(health);
            health += maxHealth / 100;
            Console.WriteLine(health);
            lastAttack = attackCooldown;
            if (health > maxHealth)
                health = maxHealth;
        }
    }

    public void DrawOptions()
    {
        patrol.Draw();
        move.Draw();
        attackMove.Draw();
        stopB.Draw();
    }

    public int CheckSelectedOptions(int x, int y)
    {
        if (stopB.CheckSelected(x, y))
        {
            SetStop();
            return 0;
        }
        if (move.CheckSelected(x, y))
            return 1;
        else if (attackMove.CheckSelected(x, y))
            return 2;
        else if (patrol.CheckSelected(x, y))
            return 3;
        // 1 - move
        // 2 - attack move
        // 3 - patrol
        return 0; 
    }

    public void LevelUP()
    {
        maxHealth += 25;
        health = maxHealth;
        xSpeed++;
        ySpeed++;
        attackDamage += 2;
        level++;
    }
}

﻿using System;
using System.IO;

public class UnitsHelpScreen
{
    Image background;
    Image transparency;
    TextButton exit;
    Font font25;
    static UnitsHelpScreen cr;
    string[] text;

    private UnitsHelpScreen()
    {
        background = new Image("data/menus/creditsMenu.jpg");
        transparency = new Image("data/misc/black.png");
        exit = new TextButton("Return", 200, 680, 100, 30, 20);
        font25 = new Font("data/fonts/joystix.ttf", 25);
        text = File.ReadAllLines("data/texts/units.txt");
    }

    public static UnitsHelpScreen GetInstance()
    {
        if (cr == null)
        {
            cr = new UnitsHelpScreen();
        }
        return cr;
    }

    public void Run()
    {

        do
        {
            SdlHardware.Pause(100);
            SdlHardware.DrawHiddenImage(background, 0, 0);
            SdlHardware.DrawHiddenImage(transparency, 0, 0);
            exit.Draw();


            for (int i = 0; i < text.Length; i++)
            {
                string line = text[i];
                SdlHardware.WriteHiddenText(
                    line
                    , (short)(50), (short)(50+i*30), 255, 255, 255, font25);
            }

            SdlHardware.ShowHiddenScreen();

            if ((exit.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
               || SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
            {
                while(SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
                {
                    ;
                }
                return;
            }


        } while (true);

    }
}

﻿using System;

class WarcraftReforged
{
    public static short WIDTH = 1366;
    public static short HEIGTH = 768;

    public static void Main(string[] args)
    {
        bool fullScreen = false;
        SdlHardware.Init(WIDTH, HEIGTH, 24, fullScreen);

        MainMenu m = new MainMenu();
        m.Run();
    }
}