﻿using System;

public class ProgressBar
{
    protected int max;
    protected int width;
    protected string line;

    Image black;

    Font font15;

    public ProgressBar(int max, int width)
    {
        this.max = max;
        this.width = width;
        font15 = new Font("data/fonts/joystix.ttf", 15);
        black = new Image("data/misc/black.png");
    }

    public void Draw(short x, short y, int actual)
    {
        line = "";
        line += new string('*',  actual/width);
        line += new string('-', (max - actual)/width);

        SdlHardware.DrawHiddenImage(black, x - 5, y - 5, (short)width, 25);
        SdlHardware.WriteHiddenText(line, x, y, 255, 255, 255, font15);
    }
}