﻿using System;

class Horseman : Unit
{
    public static int cost = 70;

    public Horseman(int x, int y, int extraDamage = 0, int extraHealth = 0,
                   string img = null) : base(x, y, 48, 48, 30, 25+extraDamage)
    {
        baseHealth = 125;
        maxHealth = baseHealth + extraHealth;
        xSpeed = ySpeed = 12;
        health = maxHealth;

        if (img == null)
        {
            LoadSequence(DOWN,
                         new string[] {
            "data/units/horseman/d1.png",
            "data/units/horseman/d2.png"}
                        );
            LoadSequence(UP,
                         new string[] {
            "data/units/horseman/u1.png",
            "data/units/horseman/u2.png"}
                        );
            LoadSequence(LEFT,
                         new string[] {
            "data/units/horseman/l1.png",
            "data/units/horseman/l2.png"}
                        );
            LoadSequence(RIGHT,
                         new string[] {
            "data/units/horseman/r1.png",
            "data/units/horseman/r2.png"}
                        );
        }
        else
            LoadImage(img);
    }
}

