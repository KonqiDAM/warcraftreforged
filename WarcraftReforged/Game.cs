﻿/**
 * Game.cs - WarcraftReforged
 * Ivan Lazcano Sindin
 *
 */

using System;
using Tao.Sdl;
using System.Collections.Generic;
using System.IO;

struct Upgrades 
{
    public int extraHealth;
    public int extraDamage;
}

class Game
{

    StreamWriter debugFile;
    ProgressBar energyBar;
    Image uiBackground;
    Image combatImg;
    DateTime lastCreatedUnit;

    Sprite actualConstructing;
    Sprite actualRecuiting;
    Map defaultMap;
    PauseMenu pause;
    EndScreen gameOverScreen;

    Font font18;

    List<Unit> units;
    List<Unit> enemyUnits;
    List<Building> buildings;
    List<Building> enemyBuildings;
    ImageButton[] menuOptions;
    ImageButton pauseButton;
    Upgrades[] unitsUpgrades;
    Random rnd;
    Unit aux;

    int enemyMinTimeCreationNewUnit;
    int xCore, yCore, yEnemyCore, xEnemyCore;
    public static short xoffSet;
    public static short yoffSet;
    short scrollSpeed;
    int actualEnergy;
    int maxEnergy;
    int actionOfUnit;

    short uiHeiht;

    bool finished;

    bool constructing;
    bool drawDefaultOptions;
    bool recuiting;
    bool movingUnit;
    int typeConstructing;
    int typeUnitToRecuit;
    public static bool debuging;

    bool debugingToFile;

    void InitEnemyInfo()
    {
        lastCreatedUnit = DateTime.Now;
        xCore = 20;
        yCore = 20;
        yEnemyCore = 1200;
        xEnemyCore = 3000;
        rnd = new Random();
    }

    void InitUnitsOfPlayer()
    {
        actionOfUnit = 0;
        units = new List<Unit>();
        units.Add(new Pyro(60, 60));
        units.Add(new Soldier(120, 60));
        units.Add(new Pyro(60, 120));
        units.Add(new Horseman(120, 120));
        enemyUnits = new List<Unit>();
        enemyUnits.Add(new Pyro(1200, 1200,0,0,"data/units/soldier.png"));
        unitsUpgrades = new Upgrades[3];

        //1 soldier
        //2 pyto
        //3 horseman

        unitsUpgrades[0].extraDamage = 0;
        unitsUpgrades[0].extraHealth = 0;
        unitsUpgrades[1].extraDamage = 0;
        unitsUpgrades[1].extraHealth = 0;
        unitsUpgrades[2].extraDamage = 0;
        unitsUpgrades[2].extraHealth = 0;
    }



    void Init()
    {
        pause = new PauseMenu();
        debuging = false;
        constructing = false;
        drawDefaultOptions = true;
        finished = false;
        recuiting = false;
        debugingToFile = false;
        finished = false;
        movingUnit = false;
        InitUnitsOfPlayer();
        try
        {
            defaultMap = new Map(1);
            defaultMap.GetCoreCordinates(
                out xCore, out  yCore, out xEnemyCore, out yEnemyCore);
            if(debuging)
                Console.WriteLine(xCore+" "+yCore+
                                  "\n"+xEnemyCore+" "+yEnemyCore);

        }
        catch (Exception)
        {
            Console.WriteLine("Exited with error on map!");
            Environment.Exit(-1);
        }

        uiBackground = new Image("data/ui/brown.jpg");
        combatImg = new Image("data/ui/combat.png");
        pauseButton = new ImageButton(
            "data/ui/menu.png", WarcraftReforged.WIDTH - 107, 0, 107, 24);
        GenerateMenuOption();

        font18 = new Font("data/fonts/joystix.ttf", 18);

        enemyMinTimeCreationNewUnit = 10;
        actualEnergy = 360;
        maxEnergy = 10000;
        energyBar = new ProgressBar(maxEnergy, 1110);
        rnd = new Random();
        gameOverScreen = EndScreen.GetInstance();

        buildings = new List<Building>
        {
            new Barrack(500, 500),
            new Barrack(2000, 1000)
        };
        enemyBuildings = new List<Building>
        {
            new Barrack(1000, 1200, "data/buildings/b2.png")
        };
        if (debugingToFile)
        {
            debugFile = new StreamWriter("debugFile.txt");
        }
        if (debuging)
            actualEnergy = 999999;

    }

    void GenerateMenuOption()
    {
        //In constructing menu
        //1 - barrack
        //2 - MineralTower
        //3 - Laboratory

        menuOptions = new ImageButton[3];
        menuOptions[0] = new ImageButton("data/ui/barracks.png",
                                         WarcraftReforged.WIDTH - 50,
                                         WarcraftReforged.HEIGTH - 50,
                                         48, 48);
        menuOptions[2] = new ImageButton("data/ui/laboratory.png",
                                         WarcraftReforged.WIDTH - 100,
                                         WarcraftReforged.HEIGTH - 50,
                                         48, 48);
        menuOptions[1] = new ImageButton("data/ui/mineral.png",
                                         WarcraftReforged.WIDTH - 150,
                                         WarcraftReforged.HEIGTH - 50,
                                         48, 48);

        uiHeiht = 120;
        xoffSet = 0;
        yoffSet = 0;
        scrollSpeed = 40;

    }

    void DrawUnitInformation(Unit u)
    {
        SdlHardware.WriteHiddenText("Type:" + u,
                                    (short)(500 - xoffSet)
                                    , (short)(655 - yoffSet),
                                    255, 255, 255, font18);
        SdlHardware.WriteHiddenText("Health:" + u.GetHealth(),
                                    (short)(500 - xoffSet),
                                    (short)(675 - yoffSet),
                                    255, 255, 255, font18);
        SdlHardware.WriteHiddenText("Level:" + u.GetLevel(),
                                    (short)(500 - xoffSet),
                                    (short)(695 - yoffSet),
                                    255, 255, 255, font18);
        SdlHardware.WriteHiddenText("Damage:" + u.GetAttackDamage(),
                                    (short)(500 - xoffSet), 
                                    (short)(715 - yoffSet),
                                    255, 255, 255, font18);
        SdlHardware.WriteHiddenText("Speed:" + u.GetSpeedX(),
                                        (short)(500 - xoffSet), 
                                        (short)(735 - yoffSet),
                                        255, 255, 255, font18);
    }

    void DrawBuildingInfomration(Building b)
    {
            if(b.IsSelected())
            {
                SdlHardware.WriteHiddenText("Type:" + b,
                                        (short)(500 - xoffSet)
                                        , (short)(655 - yoffSet),
                                        255, 255, 255, font18);
                SdlHardware.WriteHiddenText("Health:" + b.GetHealth(),
                                            (short)(500 - xoffSet),
                                            (short)(675 - yoffSet),
                                            255, 255, 255, font18);
                SdlHardware.WriteHiddenText("Range:" + b.GetVisionRange(),
                                        (short)(500 - xoffSet),
                                        (short)(695 - yoffSet),
                                        255, 255, 255, font18);

            }
    }

    bool DrawUnitsBuildingsOptions()
    {
        drawDefaultOptions = true;
        foreach (Building b in buildings)
        {
            if (b.IsSelected())
            {
                b.DrawOptions();
                DrawBuildingInfomration(b);
                drawDefaultOptions = false;
            }
        }

        foreach (Unit u in units)
        {
            if (u.IsSelected())
            {
                u.DrawOptions();
                DrawUnitInformation(u);
                drawDefaultOptions = false;
            }
        }
        return drawDefaultOptions;
    }

    void DrawUIBar()
    {
        SdlHardware.DrawHiddenImage(uiBackground, -xoffSet,
                                    650 - yoffSet, 1366, uiHeiht);

        SdlHardware.WriteHiddenText("Energy:" + actualEnergy,
                                    (short)(10 - xoffSet)
                                    , (short)(659 - yoffSet),
                                    255, 255, 255, font18);
    }

    void DrawUi()
    {
        pauseButton.Draw();

        DrawUIBar();
        drawDefaultOptions = DrawUnitsBuildingsOptions();

        //TODO fix energyBar
        //energyBar.Draw((short)(200-xoffSet), (short)(660-yoffSet),
        //               actualEnergy);

        SdlHardware.WriteHiddenText("Total units: " + units.Count,
                                    (short)(10 - xoffSet),
                                    (short)(745 - yoffSet),
                                    255, 255, 255, font18);
        //if (constructing)
            //drawDefaultOptions = false;

        if(drawDefaultOptions)
        {
            for (int i = 0; i < menuOptions.Length; i++)
            {
                menuOptions[i].Draw();
            }
        }
    }

    void DrawCombatImg(Unit u)
    {
        SdlHardware.DrawHiddenImage(
            combatImg, u.GetX() 
            + u.GetWidth()/2 - 12, u.GetY() - 26);
    }


    void CheckVisibilityOfUnits()
    {
        foreach (Unit e in enemyUnits)
            e.SetIsVisibleByEnemy(false);

        foreach (Unit u in units)
        {
            foreach (Unit e in enemyUnits)
                if (u.InVisionRange(e))
                    e.SetIsVisibleByEnemy(true);
        }

        foreach(Building b in buildings)
            foreach (Unit e in enemyUnits)
                if (b.InVisionRange(e))
                    e.SetIsVisibleByEnemy(true);

    }

    void DrawUnits()
    {
        CheckVisibilityOfUnits();
        foreach (Unit s in units)
        {
            s.DrawOnHiddenScreen();
            if (s.IsSelected())
                s.DrawOptions();
            if (s.InCombat())
            {
                DrawCombatImg(s);
            }
        }

        foreach (Unit e in enemyUnits)
        {
            if(e.IsVisibleByEnemy())
                e.DrawOnHiddenScreen();
            if (e.InCombat())
                DrawCombatImg(e);
        }

        if (debuging)
            foreach (Unit ss in units)
                if (ss.IsSelected())
                    SdlHardware.WriteHiddenText(
                    "x:" + ss.GetX() +
                    ", y: " + ss.GetY(),
                    (short)-xoffSet,
                    (short)-yoffSet,
                    125, 125, 125, font18);
    }

    void DrawBuildings()
    {
        foreach(Building b in buildings)
            b.DrawOnHiddenScreen();
        
        foreach (Building b in enemyBuildings)
            b.DrawOnHiddenScreen();
    }

    void UpdateScreen()
    {
        SdlHardware.ClearScreen();
        defaultMap.Draw(units, buildings);
        DrawUnits();
        DrawBuildings();
        DrawUi();

        if(constructing)
        {
            actualConstructing.MoveTo(SdlHardware.GetMouseX()
                              -actualConstructing.GetWidth()/2-xoffSet,
                              SdlHardware.GetMouseY()
                              -actualConstructing.GetHeight()/2-yoffSet);
            actualConstructing.DrawOnHiddenScreen();
        }

        if(recuiting)
        {
            actualRecuiting.MoveTo(
                SdlHardware.GetMouseX()
                - actualRecuiting.GetWidth() / 2 - xoffSet,
                SdlHardware.GetMouseY()
                - actualRecuiting.GetHeight() / 2 - yoffSet);
            actualRecuiting.DrawOnHiddenScreen();
        }

        SdlHardware.ShowHiddenScreen();
    }

    void CheckScroll()
    {
        if (SdlHardware.KeyPressed(SdlHardware.KEY_RIGHT)
            || SdlHardware.GetMouseX() > 1361)
        {
            if (xoffSet - scrollSpeed - WarcraftReforged.WIDTH
                >= -defaultMap.WIDTH)
            {
                SdlHardware.ScrollHorizontally((short)(-scrollSpeed));
                xoffSet -= scrollSpeed;
            }
        }

        if (SdlHardware.KeyPressed(SdlHardware.KEY_LEFT)
            || SdlHardware.GetMouseX() < 5)
        {
            if (xoffSet + scrollSpeed <= 0)
            {
                SdlHardware.ScrollHorizontally((short)(scrollSpeed));
                xoffSet += scrollSpeed;
            }
        }

        if (SdlHardware.KeyPressed(SdlHardware.KEY_UP)
            || SdlHardware.GetMouseY() < 5) 
        {
            if (yoffSet + scrollSpeed <= 0)
            {
                SdlHardware.ScrollVertically((short)(scrollSpeed));
                yoffSet += scrollSpeed;
            }
        }

        if (SdlHardware.KeyPressed(SdlHardware.KEY_DOWN)
            || SdlHardware.GetMouseY() > 762)
        {
            if (yoffSet - scrollSpeed - WarcraftReforged.HEIGTH + 
                uiHeiht  >= -defaultMap.HEIGHT)
            {
                SdlHardware.ScrollVertically((short)(-scrollSpeed));
                yoffSet -= scrollSpeed;
            }
        }
    }

    void CheckConstructionType()
    {
        switch (typeConstructing)
        {
            case 1:
                if (actualEnergy >= Barrack.cost)
                {
                    actualEnergy -= Barrack.cost;
                    buildings.Add(new Barrack(
                        actualConstructing.GetX(),
                        actualConstructing.GetY()));
                }
                break;
            case 2:
                if (actualEnergy >= MineralTower.cost)
                {
                    actualEnergy -= MineralTower.cost;
                    buildings.Add(new MineralTower(
                        actualConstructing.GetX(),
                        actualConstructing.GetY()));
                }
                break;
            case 3:
                if (actualEnergy >= Lab.cost)
                {
                    actualEnergy -= Lab.cost;
                    buildings.Add(new Lab(
                        actualConstructing.GetX(),
                        actualConstructing.GetY()));
                }
                break;
        }
    }

    void MineralTowerCons()
    {
        int tmpX = 0, tmpY = 0;
        if (defaultMap.IsMineral(actualConstructing,
                                 ref tmpX, ref tmpY))
        {
            constructing = true;
            actualConstructing.MoveTo(tmpX, tmpY);
            //we want to put the mineralTower 
            //exactly on the original sprite point
        }
        else
            constructing = false;
    }

    void CheckCollisionVisionConstructing()
    {
        foreach (Unit s in units)
        {
            if (s.CollisionsWith(actualConstructing) 
                && !s.InVisionRange(actualConstructing))
                constructing = false;
        }

        foreach (Unit e in enemyUnits)
        {
            if (e.CollisionsWith(actualConstructing))
                constructing = false;
        }

        if (defaultMap.ColisionWithMap(actualConstructing))
        {
            constructing = false;
        }
    }

    bool CheckInVision(Sprite s)
    {
        bool inVision = false;
        foreach(Unit u in units)
        {
            if (u.InVisionRange(s))
                inVision = true;
        }
        foreach(Building b in buildings)
            if (b.InVisionRange(s))
                inVision = true;

        return inVision;
    }

    void CheckConstruction()
    {
        if (typeConstructing == 2)
        {
            MineralTowerCons();
        }
        else
        {
            CheckCollisionVisionConstructing();
        }

        foreach (Building b in buildings)
            if (b.CollisionsWith(actualConstructing))
                constructing = false;

        foreach (Building b in enemyBuildings)
            if (b.CollisionsWith(actualConstructing))
                constructing = false;
        
        if (constructing && CheckInVision(actualConstructing))
        {
            CheckConstructionType();
            constructing = false;
        }
    }

    void CheckRecuitingUnitType()
    {

        //1 soldier
        //2 pyto
        //3 horseman
        switch (typeUnitToRecuit)
        {
            case 1:
                if (actualEnergy >= Soldier.cost)
                {
                    actualEnergy -= Soldier.cost;
                    units.Add(new Soldier(
                        actualRecuiting.GetX(),
                        actualRecuiting.GetY(),
                        unitsUpgrades[0].extraDamage,
                        unitsUpgrades[0].extraHealth));
                }
                break;
            case 2:
                if (actualEnergy >= Pyro.cost)
                {
                    actualEnergy -= Pyro.cost;
                    units.Add(new Pyro(
                        actualRecuiting.GetX(),
                        actualRecuiting.GetY(),
                        unitsUpgrades[1].extraDamage,
                        unitsUpgrades[1].extraHealth));
                }
                break;
            case 3:
                if (actualEnergy >= Horseman.cost)
                {
                    actualEnergy -= Horseman.cost;
                    units.Add(new Horseman(
                        actualRecuiting.GetX(),
                        actualRecuiting.GetY(),
                        unitsUpgrades[2].extraDamage,
                        unitsUpgrades[2].extraHealth));
                }
                break;
        }
        recuiting = false;
    }

    void CheckRecruitCollision()
    {
        foreach (Unit s in units)
            if (s.CollisionsWith(actualRecuiting))
                recuiting = false;

        foreach (Unit e in enemyUnits)
            if(e.CollisionsWith(actualRecuiting))
                recuiting = false;

        if (defaultMap.ColisionWithMap(actualRecuiting))
            recuiting = false;

        foreach (Building b in buildings)
            if (b.CollisionsWith(actualRecuiting))
                recuiting = false;
        
        foreach(Building b in enemyBuildings)
            if (b.CollisionsWith(actualRecuiting))
                recuiting = false;

        if (recuiting && CheckInVision(actualRecuiting))
        {
            CheckRecuitingUnitType();
        }
    }

    void MoveUnit()
    {
        foreach (Unit u in units)
        {
            if (u.IsSelected())
            {
                switch(actionOfUnit)
                {
                    case 1:
                        u.WalkTo(SdlHardware.GetMouseX() - xoffSet,
                             SdlHardware.GetMouseY() - yoffSet);
                        break;
                    case 2:
                        u.WalkTo(SdlHardware.GetMouseX() - xoffSet,
                               SdlHardware.GetMouseY() - yoffSet, false);
                        break;
                    case 3:
                        u.SetPatrolling(
                            SdlHardware.GetMouseX() - xoffSet,
                            SdlHardware.GetMouseY() - yoffSet);
                        break;
                }
            }
        }
                // 1 - move
                // 2 - atack move
                // 3 - patrol
    }

    void LaunchPause()
    {
        while (SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
        {
            ;
        }
        SdlHardware.ResetScroll();
        if (!pause.Run())
        {
            finished = true;
            yoffSet = 0;
            xoffSet = 0;
        }
        SdlHardware.ScrollTo(xoffSet, yoffSet);
    }

    void CheckLeftClick()
    {
        if(pauseButton.CheckSelected(SdlHardware.GetMouseX(),
                                     SdlHardware.GetMouseY()))
        {
            LaunchPause();
        }
        if (movingUnit)
            MoveUnit();
        foreach (Building b in buildings)
            b.CheckSelected(SdlHardware.GetMouseX() - xoffSet,
                        SdlHardware.GetMouseY() - yoffSet);
        
        if (constructing)
            CheckConstruction();

        if(recuiting)
            CheckRecruitCollision();

        foreach (Unit s in units)
            if (s.CheckSelected(SdlHardware.GetMouseX() - xoffSet,
                                SdlHardware.GetMouseY() - yoffSet))
                s.MoveTo(s.GetX(), s.GetY());
        
        movingUnit = false;
    }

    int SelectedUnit()
    {
        //The game was planed to have the ability to select multiple units
        //but not now :)
        for (int i = 0; i < units.Count; i++)
        {
            if (units[i].IsSelected())
                return i;
        }
        return 0;
    }

    void EscPressed()
    {
        if (!constructing && !recuiting)
        {
            LaunchPause();
        }
        else
        {
            constructing = false;
            recuiting = false;
        }
    }

    void CheckKeys()
    {
        int selectedUnit = SelectedUnit();

        if (SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
            EscPressed();
        
        if (SdlHardware.KeyPressed(SdlHardware.KEY_B))
            Construct(1);
        
        if (SdlHardware.KeyPressed(SdlHardware.KEY_M))
            Construct(2);

        if (SdlHardware.KeyPressed(SdlHardware.KEY_L))
            Construct(3);
       
        if(SdlHardware.KeyPressed(SdlHardware.KEY_P))
            units[selectedUnit].SetPatrolling(
                SdlHardware.GetMouseX() - xoffSet,
                SdlHardware.GetMouseY() - yoffSet);
            
        if (SdlHardware.KeyPressed(SdlHardware.KEY_A))
            units[selectedUnit].WalkTo(SdlHardware.GetMouseX() - xoffSet,
                             SdlHardware.GetMouseY() - yoffSet, false);

        if (SdlHardware.KeyPressed(SdlHardware.KEY_S))
            units[selectedUnit].SetStop();
    }

    void CheckUpgrades(int returned)
    {
        switch (returned)
        {
            case 0:
                break;
            case 1:
                unitsUpgrades[0].extraHealth += 25;
                unitsUpgrades[1].extraHealth += 15;
                unitsUpgrades[2].extraHealth += 20;
                break;
        }
    }

    void CheckRecuitment(int returned)
    {
        switch (returned)
        {
            case 0:
                break;
            case 1:
                recuiting = true;
                typeUnitToRecuit = 1;
                actualRecuiting = new Sprite(
                    "data/units/soldier/d1.png", 48, 48);
                if(debuging)
                    Console.WriteLine("soldier");
                break;
            case 2:
                recuiting = true;
                typeUnitToRecuit = 2;
                actualRecuiting = new Sprite(
                    "data/units/pyro/d1.png", 48, 48);
                break;
            case 3:
                recuiting = true;
                typeUnitToRecuit = 3;
                actualRecuiting = new Sprite(
                    "data/units/horseman/d1.png", 48, 48);
                break;
        }
    }

    void CheckConstructingMenu()
    {
        for (int i = 0; i < menuOptions.Length; i++)
        {
            menuOptions[i].CheckSelected(
            SdlHardware.GetMouseX(),
            SdlHardware.GetMouseY());
            if (menuOptions[i].IsSelected())
            {
                //1 - barrack
                //2 - MineralTower
                //3 - Laboratory
                switch (i)
                {
                    case 0:
                        Construct(1);
                     break;
                    case 1:
                        Construct(2);
                        break;
                    case 2:
                        Construct(3);
                        break;
                }
            }
        }
    }

    void CheckBuildingOptions()
    {
        foreach (Building b in buildings)
        {
            if (b.IsSelected())
            {
                int returned =
                        b.CheckSelectedOptions(
                        SdlHardware.GetMouseX(),
                        SdlHardware.GetMouseY());
                if (b.GetType() == typeof(Lab))
                {
                    CheckUpgrades(returned);
                }
                else if (b.GetType() == typeof(Barrack))
                {
                    CheckRecuitment(returned);
                }
            }
        }
    }

    void CheckUnitOptions()
    {
        foreach(Unit u in units)
        {
            if(u.IsSelected())
            {
                actionOfUnit =
                        u.CheckSelectedOptions(
                        SdlHardware.GetMouseX(),
                        SdlHardware.GetMouseY());
                if(actionOfUnit != 0)
                {
                    movingUnit = true;
                }
            }
        }
    }

    void CheckUiClick()
    {
        if (!drawDefaultOptions)
        {
            CheckBuildingOptions();
            CheckUnitOptions();
        }
        else
        {
            CheckConstructingMenu();
        }
    }

    void CheckUserInput()
    {
        CheckScroll();
        CheckKeys();

        if (SdlHardware.MouseClicked(1))
        {
            if (WarcraftReforged.HEIGTH -
                SdlHardware.GetMouseY() < uiHeiht)
            {
                CheckUiClick();
            }
            else
                CheckLeftClick();
        }

        if (SdlHardware.MouseClicked(2))
            foreach (Unit s in units)
                if (s.IsSelected())
                    s.WalkTo(SdlHardware.GetMouseX() - xoffSet, 
                             SdlHardware.GetMouseY() - yoffSet);
        
    }

    bool IsOutOfMap(Sprite u)
    {
        if(u.GetX() < 0 || u.GetY() < 0 ||
           u.GetX() > defaultMap.WIDTH ||
           u.GetY() > defaultMap.HEIGHT)
        {
            return false;
        }
        return true;
    }

    bool CollidesWithObjet(Unit i)
    {
        foreach (Building b in enemyBuildings)
            if (b.CollisionsWith(i))
                return true;
        
        foreach (Building b in buildings)
            if (b.CollisionsWith(i))
                return true;

        for (int j = 0; j < units.Count; j++)
        {
            if (units[j] == i)//we dont want to check colision with him self
                continue;
            if (Math.Abs(i.GetX() - units[j].GetX()) < 130 &&
                i.CollisionsWith(units[j]))
            return true;
        }

        foreach (Unit e in enemyUnits)
            if (i.CollisionsWith(e) && e != i)
                return true;

        if (defaultMap.ColisionWithMap(i))
            return true;

        return !IsOutOfMap(i);
    }

    void MoveUnits(List<Unit> soldiers)
    {
        //we try to move all soldiers, if they colide with another sprite
        //they cant move to that position
        int oldX, oldY;

        for (int i = 0; i < soldiers.Count; i++)
        {
            oldX = soldiers[i].GetX();
            oldY = soldiers[i].GetY();

            soldiers[i].MoveX();

            if (CollidesWithObjet(soldiers[i]))
                soldiers[i].MoveTo(oldX, oldY);

            soldiers[i].MoveY();

            if (CollidesWithObjet(soldiers[i]))
                soldiers[i].MoveTo(oldX, oldY);

        }
    }

    void Atack(List<Unit> units, List<Unit> enemyUnits,
               List<Building> enemyBuildings)
    {
        foreach (Unit u in units)
        {
            foreach (Building b in enemyBuildings)
                if (u.CanAttack(b))
                {
                    u.Attack(b);
                    if (b.GetHealth() < 0)
                        u.LevelUP();
                }

            foreach (Unit e in enemyUnits)
                if (u.CanAttack(e))
                {
                    u.Attack(e);
                    if (e.GetHealth() < 0)
                        u.LevelUP();
                }
        }
    }

    void Combat()
    {
        Atack(enemyUnits, units, buildings);
        Atack(units, enemyUnits, enemyBuildings);
    }

    void RemoveSprite(List<Unit> s)
    {
        int countDeleted = 0;
        for (int i = 0; i < s.Count - countDeleted; i++)
        {
            if (s[i].GetHealth() <= 0)
            {
                s.Remove(s[i]);
                countDeleted++;
            }
        }
    }

    void RemoveBuildings(List<Building> s)
    {
        int countDeleted = 0;
        for (int i = 0; i < s.Count - countDeleted; i++)
        {
            if (s[i].GetHealth() <= 0)
            {
                s.Remove(s[i]);
                countDeleted++;
            }
        }
    }

    void RemoveDeadSoldier()
    {
        RemoveSprite(units);
        RemoveSprite(enemyUnits);
    }

    void RemoveDestructedBuildings()
    {
        RemoveBuildings(buildings);
        RemoveBuildings(enemyBuildings);
    }

    void UpdateWorld()
    {
        foreach (Building b in buildings)
        {
            if (b.GetType() == typeof(MineralTower))
            {
                actualEnergy += 1;
                if (actualEnergy > maxEnergy)
                    actualEnergy = maxEnergy;
            }
        }
        RemoveDeadSoldier();
        RemoveDestructedBuildings();
    }

    void Construct(int type)
    {
        //1 - barrack
        //2 - MineralTower
        //3 - Laboratory
        typeConstructing = type;
        switch(type)
        {
            case 1:
                actualConstructing = new Sprite("data/buildings/" +
                                                "barracks.png", 96, 96);
                constructing = true;
                break;
            case 2:
                actualConstructing = new Sprite("data/buildings/" +
                                                "mineralTower.png", 48, 48);
                constructing = true;
                break;
            case 3:
                actualConstructing = new Sprite("data/buildings/" +
                                                "laboratory.png", 96, 96);
                constructing = true;
                break;
        }
    }

    void DebugToFile()
    {
        debugFile.WriteLine("Energy: " + actualEnergy);
    }

    void RefreshCoolDownUnits()
    {
        foreach (Unit u in units)
            u.RefreshAttackColldown();
        foreach (Unit e in enemyUnits)
            e.RefreshAttackColldown();
    }

    bool CanCreateNewUnit()
    {
        if ((DateTime.Now - lastCreatedUnit).TotalSeconds
            > enemyMinTimeCreationNewUnit)
        {
            lastCreatedUnit = DateTime.Now;
            return true;
        }
        return false;
    }

    void RecruitNewUnit()
    {
        if (rnd.Next() % 2 == 0)
        {
            if (rnd.Next() % 2 == 0)
            {
                aux = new Soldier(xEnemyCore, yEnemyCore,
                                       0, 0, "data/units/s.png");
            }
            else
            {
                aux = new Pyro(xEnemyCore, yEnemyCore,
                                    0, 0, "data/units/soldier.png");
            }
            if(debuging)
                Console.WriteLine("Try to create a " + aux);
            bool done = false;
            for (int i = 0; i < 15 && !done ; i++)
            {
                aux.MoveTo(rnd.Next(xEnemyCore - 500, xEnemyCore + 500),
                            rnd.Next(yEnemyCore - 300, yEnemyCore + 300));
                if(!CollidesWithObjet(aux))
                {
                    done = true;
                    if(debuging)
                        Console.WriteLine("Done");
                    enemyUnits.Add(aux);
                }
            }
        }
    }

    void ExecuteEnemyAI()
    {
        if (CanCreateNewUnit())
        {
            RecruitNewUnit();
        }
        if(enemyUnits.Count > 15)
        {
            if(debuging)
                Console.WriteLine("Atacking core!");
            foreach(Unit u in enemyUnits)
            {
                u.WalkTo(xCore,yCore);
            }
        }
    }

    void GoToEnemy(List<Unit> units, List<Unit> enemyUnits)
    {
        foreach (Unit u in units)
            foreach (Unit e in enemyUnits)
            {
                if (u.InVisionRange(e) && !u.IsStopped())
                    u.WalkTo(e.GetX() - e.GetWidth() / 2,
                             e.GetY() - e.GetHeight() / 2,
                             false);
                
                if (e.InVisionRange(u))
                    e.WalkTo(u.GetX() - u.GetWidth() / 2,
                         u.GetY() - u.GetHeight() / 2,
                             false);
            }
    }

    void HealUnits(List<Unit> units)
    {
        foreach(Unit u in units)
        {
            u.HealIfOutOfCombat();
        }
    }

    void AImovements()
    {
        RefreshCoolDownUnits();
        ExecuteEnemyAI();
        GoToEnemy(units, enemyUnits);
        GoToEnemy(enemyUnits, units);
        MoveUnits(units);
        MoveUnits(enemyUnits);
        Combat();
        HealUnits(units);
        HealUnits(enemyUnits);
    }

    void PauseUntilNextFrame()
    {
        SdlHardware.Pause(30);
    }

    void CheckWinCondition()
    {
        if (buildings.Count == 0)
        {
            finished = true;
            yoffSet = 0;
            xoffSet = 0;
            SdlHardware.ResetScroll();
            gameOverScreen.Run();
        }
        else if(enemyBuildings.Count == 0)
        {
            finished = true;
            yoffSet = 0;
            xoffSet = 0;
            SdlHardware.ResetScroll();
            gameOverScreen.Run();

            //TODO Launch win screen

        }
    }

    public void Run()
    {
        Init();

        do
        {
            AImovements();
            CheckUserInput();
            UpdateWorld();
            UpdateScreen();
            PauseUntilNextFrame();
            if (debugingToFile)
                DebugToFile();
            CheckWinCondition();
        }
        while (!finished);
        if(debugingToFile)
        {
            debugFile.Close();
        }
    }
}