﻿using System;


abstract class Unit : Sprite
{
    Font font18;
    bool isPatrolling;
    bool stop;
    bool isCombating;
    bool isVisibleByEnemy;
    protected bool selected;
    protected int walkToX;
    protected int walkToY;
    protected int range, attackDamage, level;
    int xInitPatrol, yInitPatrol, yFinalPatrol, xFinalPatrol;
    protected ImageButton patrol;
    protected ImageButton move;
    protected ImageButton attackMove;
    protected ImageButton stopB;
    protected int attackCooldown;
    protected int lastAttack;
    protected int maxHealth;
    protected static int baseHealth;

    public Unit(int x, int y, int width, int height, int range,
                int attackDamage) : base(width, height)
    {
        walkToX = this.x = x;
        walkToY = this.y = y;
        font18 = new Font("data/fonts/joystix.ttf", 18);
        currentDirection = DOWN;
        visionRange = 350;
        this.range = range;
        this.attackDamage = attackDamage;
        level = 1;
        stop = false;
        attackCooldown = 15;
        lastAttack = 0;
        isCombating = false;

        patrol = new ImageButton("data/ui/patrol.png",
                                WarcraftReforged.WIDTH - 50,
                                WarcraftReforged.HEIGTH - 50,
                                48, 48);

        move = new ImageButton("data/ui/move.png",
                                WarcraftReforged.WIDTH - 100,
                                WarcraftReforged.HEIGTH - 50,
                                48, 48);

        attackMove = new ImageButton("data/ui/atackMove.png",
                                WarcraftReforged.WIDTH - 150,
                                WarcraftReforged.HEIGTH - 50,
                                48, 48);

        stopB = new ImageButton("data/ui/stop.png",
                                WarcraftReforged.WIDTH - 200,
                                WarcraftReforged.HEIGTH - 50,
                                48, 48);
    }


    public int GetLevel() { return level; }
    public int GetAttackDamage() { return attackDamage; }
    public int GetRange() { return range; }

    public bool IsVisibleByEnemy() { return isVisibleByEnemy; }
    public void SetIsVisibleByEnemy( bool visible ) 
    { 
        this.isVisibleByEnemy = visible;
    }
    public bool IsStopped() { return stop; }
    public bool InCombat() { return isCombating; }
    public bool IsSelected() { return selected; }

    public void SetStop() { this.stop = true; WalkTo(x,y); }

    public void SetPatrolling(int x, int y)
    {
        xInitPatrol = this.x;
        yInitPatrol = this.y;
        xFinalPatrol = x;
        yFinalPatrol = y;
        isPatrolling = true;
    }


    public bool CheckSelected(int x, int y)
    {
        if (Game.debuging)
        {
            SdlHardware.WriteHiddenText("A", (short)(this.x),
                        (short)(this.y), 255, 255, 255, font18);

            SdlHardware.WriteHiddenText("E", (short)(this.x + width),
                        (short)(this.y + height), 255, 255, 255, font18);
            SdlHardware.ShowHiddenScreen();
        }

        if (this.x <= x
            && this.x + width >= x
            && this.y <= y
            && this.y + height >= y
        )
            selected = true;
        else
            selected = false;
        return selected;
    }

    public void WalkTo(int x, int y, bool forced = true)
    {
        stop = false;
        //only move to enemy if terminated forced move by user
        if ((!forced && walkToX == this.x && walkToY == this.y) || forced)
        {
            walkToX = x;
            walkToY = y;
            isPatrolling = false;
        }
    }

    public void MoveX()
    {
        if (isPatrolling)
            Patrol();
        if (Math.Abs(x - walkToX) <= xSpeed)
            x = walkToX;
        else
        {
            if (x < walkToX)
                MoveRight();
            else
                MoveLeft();
        }
    }

    public void MoveY()
    {
        if (isPatrolling)
            Patrol();
        if (Math.Abs(y - walkToY) <= ySpeed)
            y = walkToY;
        else
        {
            if (y < walkToY)
                MoveDown();
            else
                MoveUp();
        }
    }

    protected void Patrol()
    {
        if (x == xInitPatrol && y == yInitPatrol)
        {
            walkToX = xFinalPatrol;
            walkToY = yFinalPatrol;
        }
        else if (x == xFinalPatrol && y == yFinalPatrol)
        {
            walkToX = xInitPatrol;
            walkToY = yInitPatrol;
        }
    }

    protected void MoveRight()
    {
        if (containsSequence)
        {
            ChangeDirection(RIGHT);
            NextFrame();
        }
        x += xSpeed;
    }

    protected void MoveLeft()
    {
        if (containsSequence)
        {
            ChangeDirection(LEFT);
            NextFrame();
        }
        x -= xSpeed;
    }

    protected void MoveUp()
    {
        if (containsSequence)
        {
            ChangeDirection(UP);
            NextFrame();
        }
        y -= ySpeed;
    }

    protected void MoveDown()
    {
        if (containsSequence)
        {
            ChangeDirection(DOWN);
            NextFrame();
        }
        y += ySpeed;
    }

    public void ReceiveAttack(int damage, int level)
    {
        int levelDiference = this.level - level;
        if (levelDiference > 5)
            return;
        else if (levelDiference < 5)
            health -= damage + levelDiference * 2;
        else
        {
            health -= new Random().Next(0, 10) < 8 ? damage : 0;
        }
    }

    public bool CanAttack(Sprite s)
    {
        if(s.CollisionsWith(x-range,y-range,x+range+width,y+range+height))
        {
            isCombating = true;
        }
        else
            isCombating = false;
        return isCombating;
    }

    public void Attack(Unit u)
    {
        if (lastAttack <= 0)
        {
            u.ReceiveAttack(attackDamage, level);
            lastAttack = attackCooldown;
        }
    }

    public void Attack(Building u)
    {
        if (lastAttack <= 0)
        {
            u.ReceiveAttack(attackDamage);
            lastAttack = attackCooldown;
        }
    }

    public void RefreshAttackColldown()
    {
        if (lastAttack > 0)
            lastAttack--;
    }

    public void HealIfOutOfCombat()
    {
        if (lastAttack <= 0 && health < maxHealth)
        {
            health += maxHealth / 100;
            lastAttack = attackCooldown;
            if (health > maxHealth)
                health = maxHealth;
        }
    }

    public void DrawOptions()
    {
        patrol.Draw();
        move.Draw();
        attackMove.Draw();
        stopB.Draw();
    }

    public int CheckSelectedOptions(int x, int y)
    {
        if (stopB.CheckSelected(x, y))
        {
            SetStop();
            return 0;
        }
        if (move.CheckSelected(x, y))
            return 1;
        else if (attackMove.CheckSelected(x, y))
            return 2;
        else if (patrol.CheckSelected(x, y))
            return 3;
        // 1 - move
        // 2 - attack move
        // 3 - patrol
        return 0; 
    }

    public void LevelUP()
    {
        maxHealth += 25;
        health = maxHealth;
        xSpeed++;
        ySpeed++;
        attackDamage += 2;
        level++;
    }
}

