﻿using System;

abstract class Building : Sprite
{
    protected bool selected;
    protected Font font18;

    public bool IsSelected() { return selected; }

    public Building(int x, int y, string img, int width, int height)
        : base(width, height)
    {
        LoadImage(img);
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        font18 = new Font("data/fonts/joystix.ttf", 18);
        visionRange = 400;
    }

    public int GetVisionRange() { return visionRange; }

    public bool CheckSelected(int x, int y)
    {
        if (Game.debuging)
        {
            SdlHardware.WriteHiddenText("A", (short)(this.x), 
                        (short)(this.y), 255, 255, 255, font18);

            SdlHardware.WriteHiddenText("e", (short)(this.x + width),
                        (short)(this.y + height), 255, 255, 255, font18);
            SdlHardware.ShowHiddenScreen();
        }

        if (this.x <= x
            && this.x + width >= x
            && this.y <= y
            && this.y + height >= y
        )
            selected = true;
        else
            selected = false;
        return selected;
    }

    virtual public int CheckSelectedOptions(int x, int y)
    {
        return 0;
    }

    virtual public void DrawOptions()
    {
        
    }

    public void ReceiveAttack(int damage)
    {
        health -= damage;
        if (health <= 0)
            visible = false;
    }
}

