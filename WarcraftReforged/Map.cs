﻿using System.IO;
using System;
using System.Collections.Generic;

class Map
{
    Sprite fog;
    protected SortedList<int, string> mapName;
    protected string file;
    protected Image grass, water, mineral,
    ground1, ground2, mineralTower, nexus, enemyCore,
    transparent;
    bool tileInVision;

    protected string[] map;
    protected int tileWidth;
    public int WIDTH { get; }
    public int HEIGHT { get; }

    public Map(int fileName)
    {
        mapName = new SortedList<int, string>();
        mapName.Add(1, "data/maps/1.map");
        mapName.Add(2, "data/maps/2.map");
        file = mapName[fileName];
        //bool mapReaded = true;
        transparent = new Image("data/misc/transparent.png");//s
        nexus = new Image("data/buildings/Nexus.png");//W
        grass = new Image("data/ground/grass1.png");//G
        water = new Image("data/ground/water1.png");//G
        mineral = new Image("data/buildings/mineral.png");//M
        ground1 = new Image("data/ground/ground1.png");//H
        ground2 = new Image("data/ground/ground2.png");//J
        mineralTower = new Image("data/buildings/mineralTower.png");//W
        enemyCore = new Image("data/buildings/E.png");
        tileWidth = 48;
        fog = new Sprite("data/ground/blackTransparent.png",
                         tileWidth, tileWidth);


        if(!File.Exists(file))
        {
            Console.WriteLine("Map file does not exist!");
            throw new Exception("File error!");
        }
        else
        {
            map = File.ReadAllLines(file);
        }

        HEIGHT = tileWidth * map.Length;
        WIDTH = tileWidth * map[0].Length;
    }

    public void GetCoreCordinates( out int x, out int y,
                                  out int xe, out int ye)
    {
        x = y = xe = ye = 0;
        for (int i = 0; i < map.Length; i++)
        {
            for (int j = 0; j < map[i].Length; j++)
            {
                switch (map[i][j])
                {
                    case 'S':
                        x = i * tileWidth;
                        y = j * tileWidth;
                        break;
                    case 'E':
                        xe = i * tileWidth;
                        ye = j * tileWidth;
                        break;
                }
            }
        }
    }


    public void DrawFog(List<Unit> units, 
                        List<Building> buildings, 
                        int x, int y)
    {
        tileInVision = false;

        fog.MoveTo(x,y);
        for (int i = 0; i < units.Count && !tileInVision; i++)
            if (units[i].InVisionRange(fog))
                tileInVision = true;
            
        for (int i = 0; i < buildings.Count && !tileInVision; i++)
            if (buildings[i].InVisionRange(fog))
                tileInVision = true;

        if (!tileInVision)
            fog.DrawOnHiddenScreen();
    }

    public void Draw(List <Unit> units, List<Building> buildings)
    {
        int posX, posY;
        Image aux;
        for (int y = 0; y < map.Length; y++)
        {
            for (int x = 0; x < map[y].Length; x++)
            {
                posX = x * tileWidth;
                posY = y * tileWidth;

                switch (map[y][x])
                {
                    case 'W':
                        aux = water;
                        break;
                    case 'S':
                        aux = nexus;
                        break;
                    case 'e':
                    case 's':
                        aux = transparent;
                        break;
                    case 'M':
                        aux = mineral;
                        break;
                    case 'E':
                        aux = enemyCore;
                        break;
                    default:
                        aux = grass;
                        break;
                }
                SdlHardware.DrawHiddenImage(aux, posX, posY);
                DrawFog(units, buildings, posX, posY);
            }
        }
    }

    public bool IsMineral(Sprite tower, ref int x, ref int y)
    {
        if(map[(tower.GetY()+tower.GetHeight()/2) /
               tileWidth][(tower.GetX()+tower.GetWidth()/2) /
                          tileWidth] == 'M')
        {
            x = (tower.GetX()+ tower.GetHeight() / 2) / tileWidth * tileWidth;
            y = (tower.GetY()+ tower.GetWidth() / 2) / tileWidth * tileWidth;
            //we want to return the position of the sprite 
            return true;
        }
        return false;
    }

    public bool ColisionWithMap(Sprite unit, char tileChar = ' ')
    {
        int x2tile, y2tile,x1tile, y1tile;
        for (int i = 0; i < map[0].Length; i++)
        {
            for (int j = 0; j < map.Length; j++)
            {
                char tile = map[j][i];
                if(tile != tileChar)
                {
                    x1tile = i * tileWidth;
                    y1tile = j * tileWidth;
                    x2tile = x1tile + tileWidth;
                    y2tile = y1tile + tileWidth;

                    if((x1tile < unit.GetX()+unit.GetWidth()) &&
                       (x2tile > unit.GetX()) &&
                       (y1tile < unit.GetY()+unit.GetHeight()) &&
                       (y2tile > unit.GetY()))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

