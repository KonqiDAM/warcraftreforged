﻿using System;

class ImageButton : Sprite
{
    protected bool selected;
    public ImageButton(String img, int x, int y, int width, int height) : base(width, height)
    {
        LoadImage(img);
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        selected = false;
    }

    public bool IsSelected() { return selected; }

    public bool CheckSelected(int x, int y)
    {
        if (
           this.x <= x
            && this.x + width >= x
            && this.y <= y
            && this.y + height >= y)
        {
            selected = true;
        }
        else
        {
            selected = false;
        }
        return selected;
    }

    public void Draw()
    {
        SdlHardware.DrawHiddenImage(image, x-Game.xoffSet, y-Game.yoffSet);
    }
}

