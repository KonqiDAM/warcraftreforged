﻿using System;
using System.IO;

public class BuildingsHelpScrren
{
    Image background;
    Image transparency;
    TextButton exit;
    Font font25;
    static BuildingsHelpScrren cr;
    string[] text;

    private BuildingsHelpScrren()
    {
        background = new Image("data/menus/creditsMenu.jpg");
        transparency = new Image("data/misc/black.png");
        exit = new TextButton("Return", 200, 680, 100, 30, 20);
        font25 = new Font("data/fonts/joystix.ttf", 25);
        text = File.ReadAllLines("data/texts/buildings.txt");
    }

    public static BuildingsHelpScrren GetInstance()
    {
        if (cr == null)
        {
            cr = new BuildingsHelpScrren();
        }
        return cr;
    }

    public void Run()
    {

        do
        {
            SdlHardware.Pause(100);
            SdlHardware.DrawHiddenImage(background, 0, 0);
            SdlHardware.DrawHiddenImage(transparency, 0, 0);
            exit.Draw();


            for (int i = 0; i < text.Length; i++)
            {
                string line = text[i];
                SdlHardware.WriteHiddenText(
                    line
                    , (short)(50), (short)(50+i*30), 255, 255, 255, font25);
            }

            SdlHardware.ShowHiddenScreen();

            if ((exit.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
               || SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
            {
                while (SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
                {
                    ;
                }
                return;
            }
        } while (true);
    }
}

