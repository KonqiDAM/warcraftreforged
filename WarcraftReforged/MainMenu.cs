﻿using System;

public class MainMenu
{
    Image background;
    TextButton singlePLayer;
    TextButton multiplayer;
    TextButton credits;
    TextButton help;
    TextButton exit;

    bool continueGame = true;

    Game g;
    Credits c;
    HelpScreen h;

    public MainMenu()
    {
        background = new Image("data/menus/mainMenu.jpg");
        singlePLayer = new TextButton("Single Player", 200, 200, 205, 30, 20);
        multiplayer = new TextButton("Multiplayer", 200, 250, 175, 30, 20);
        credits = new TextButton("Credits", 200, 300, 120, 30, 20);
        help = new TextButton("Tutorial/Help", 200, 350, 210, 30, 20);
        exit = new TextButton("Exit game", 200, 400, 145, 30, 20);
        g = new Game();
        c = Credits.GetInstance();
        h = HelpScreen.GetInstance();
    }

    public void Run()
    {
        do
        {
            SdlHardware.DrawHiddenImage(background, 0, 0);
            singlePLayer.Draw();
            multiplayer.Draw();
            credits.Draw();
            help.Draw();
            exit.Draw();
            SdlHardware.ShowHiddenScreen();
            if (singlePLayer.CheckSelected(SdlHardware.GetMouseX(),
                                          SdlHardware.GetMouseY())
               && SdlHardware.MouseClicked(1))
            {
                g.Run();
            }
            else if (exit.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                continueGame = false;
            }
            else if (credits.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                c.Run();
            }
            else if (help.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1))
            {
                h.Run();
            }
            SdlHardware.Pause(50);
        } while (continueGame);
    }
}