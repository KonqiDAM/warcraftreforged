﻿using System;

class WarcraftReforged
{
    public static short WIDTH = 1366;
    public static short HEIGTH = 768;

    public static void Main(string[] args)
    {
        bool fullScreen = false;
        SdlHardware.Init(WIDTH, HEIGTH, 24, fullScreen);

        MainMenu m = new MainMenu();
        m.Run();
    }
}