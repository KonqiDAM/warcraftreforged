﻿using System;

class Barrack : Building
{
    public static int cost = 400;

    ImageButton soldier, horseman, pyro;

    public Barrack(int x, int y, string img = "data/buildings/barracks.png") 
        : base(x, y, img, 96, 96)
    {
        soldier = new ImageButton("data/ui/infantery.png",
                              WarcraftReforged.WIDTH - 50,
                              WarcraftReforged.HEIGTH - 50, 48, 48);
        horseman = new ImageButton("data/ui/horseman.png",
                               WarcraftReforged.WIDTH - 100,
                              WarcraftReforged.HEIGTH - 50, 48, 48);
        pyro = new ImageButton("data/ui/archer.png",
                             WarcraftReforged.WIDTH - 150,
                              WarcraftReforged.HEIGTH - 50, 48, 48);
        health = 1600;
    }

    override public void DrawOptions()
    {
        soldier.Draw();
        horseman.Draw();
        pyro.Draw();
    }

    public override int CheckSelectedOptions(int x, int y)
    {
        if (soldier.CheckSelected(x, y))
        {
            return 1;
        }
        if (horseman.CheckSelected(x, y))
        {
            return 3;
        }
        if (pyro.CheckSelected(x, y))
        {
            return 2;
        }
        return 0;
    }
}

