﻿using System;

class Pyro : Unit
{
    public static int cost = 30;

    public Pyro(int x, int y, int extraDamage = 0, int extraHealth = 0,
                string img = null) : base(x, y, 33, 48, 30, 35+extraDamage)
    {
        baseHealth = 175;
        maxHealth = baseHealth + extraHealth;
        xSpeed = 4;
        ySpeed = 4;
        health = maxHealth;

        if (img == null)
        {
            LoadSequence(DOWN,
                         new string[] {
            "data/units/pyro/d1.png",
            "data/units/pyro/d2.png",
            "data/units/pyro/d3.png"}
                        );
            LoadSequence(UP,
                         new string[] {
            "data/units/pyro/u1.png",
            "data/units/pyro/u2.png",
            "data/units/pyro/u3.png"}
                        );
            LoadSequence(LEFT,
                         new string[] {
            "data/units/pyro/l1.png",
            "data/units/pyro/l2.png",
            "data/units/pyro/l3.png"}
                        );
            LoadSequence(RIGHT,
                         new string[] {
            "data/units/pyro/r1.png",
            "data/units/pyro/r2.png",
            "data/units/pyro/r3.png"}
                        );
        }
        else
        {
            LoadImage(img);
        }
    }
}

