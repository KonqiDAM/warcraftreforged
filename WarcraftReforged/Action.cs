﻿using System;

public class Action
{
    protected string text;
    protected DateTime creationTime;
    public Action(string text)
    {
        this.text = text;
        creationTime = DateTime.Now;
    }

    public string GetText() { return text; }
    public DateTime GetCreationTime() { return creationTime; }
}

