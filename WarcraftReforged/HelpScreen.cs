﻿using System;
using System.IO;
public class HelpScreen
{
    Image background;
    Image transparency;
    TextButton exit;
    TextButton unitsHelp;
    TextButton buildingHelp;

    UnitsHelpScreen uHelp;
    BuildingsHelpScrren bHelp;

    Font font25;
    Font font20;
    static HelpScreen hlp;

    private HelpScreen()
    {
        background = new Image("data/menus/helpMenu.png");
        transparency = new Image("data/misc/black.png");
        exit = new TextButton("Return", 200, 680, 100, 30, 20);
        unitsHelp = new TextButton("Unit help", 200, 600, 145, 30, 20);
        buildingHelp = new TextButton("Building help", 200, 640, 215, 30, 20);
        font25 = new Font("data/fonts/joystix.ttf", 25);
        font20 = new Font("data/fonts/joystix.ttf", 20);
        uHelp = UnitsHelpScreen.GetInstance();
        bHelp = BuildingsHelpScrren.GetInstance();
    }

    public static HelpScreen GetInstance()
    {
        if(hlp == null)
        {
            hlp = new HelpScreen();
        }
        return hlp;
    }

    public void Run(string file = "data/texts/help.txt")
    {
        SdlHardware.Pause(100);
        do
        {
            SdlHardware.DrawHiddenImage(background, 0, 0);
            SdlHardware.DrawHiddenImage(transparency, 0, 0);
            exit.Draw();
            unitsHelp.Draw();
            buildingHelp.Draw();
            if(!File.Exists(file))
            {
                Console.WriteLine("Help menu file does not exist!");
                Environment.Exit(-1);
            }

            try
            {
                StreamReader helpText = new StreamReader(file);
                short posX = 150;
                short posY = 100;
                string line = helpText.ReadLine();
                while(line != null)
                {
                    if (line.Contains("//"))
                    {
                        line = line.Substring(0, line.IndexOf("//"));
                    }
                    if(line.Contains(":"))
                    {
                        SdlHardware.WriteHiddenText(
                            line
                            , (short)(posX-50), posY, 255, 255, 255, font25);
                    }
                    else
                    {
                        SdlHardware.WriteHiddenText(
                            line
                            , posX, posY, 255, 255, 255, font25);
                    }
                    posY += 30;
                    line = helpText.ReadLine();
                }

                helpText.Close();
            }
            catch (PathTooLongException)
            {
                Console.WriteLine("Path to long!");
                Environment.Exit(-1);
            }
            catch (IOException)
            {
                Console.WriteLine("Help menu text file error!");
                Environment.Exit(-1);
            }
            catch (Exception)
            {
                Console.WriteLine("Error on file reading!");
                Environment.Exit(-1);
            }


            SdlHardware.ShowHiddenScreen();


                if (( exit.CheckSelected(SdlHardware.GetMouseX(),
                                        SdlHardware.GetMouseY())
                     && SdlHardware.MouseClicked(1))
                    || SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
                {
                    while (SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
                    {
                        ;
                    }
                    return;
                }
                else if(unitsHelp.CheckSelected(SdlHardware.GetMouseX(),
                                                SdlHardware.GetMouseY()) 
                        && SdlHardware.MouseClicked(1))
                {
                    uHelp.Run();
                }
                else if(buildingHelp.CheckSelected(SdlHardware.GetMouseX(),
                                                   SdlHardware.GetMouseY())
                            && SdlHardware.MouseClicked(1))
                {
                    bHelp.Run();
                }
        } while (true);
    }
}

