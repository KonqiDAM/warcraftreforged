﻿using System;

class Lab : Building
{
    public static int cost = 100;
    protected ImageButton medic;

    public Lab(int x, int y,
               string img = "data/buildings/laboratory.png") 
        : base(x, y, img, 96, 96)
    {
        medic = new ImageButton("data/ui/medic.png",
                              WarcraftReforged.WIDTH - 50,
                              WarcraftReforged.HEIGTH - 50,
                              48, 48);
        health = 1100;
    }

    public override void DrawOptions()
    {
        medic.Draw();
    }

    public override int CheckSelectedOptions(int x, int y)
    {
        if (medic.CheckSelected(x, y))
        {
            if(Game.debuging)
                Console.WriteLine("upgrade!");
            return 1;
        }
        return 0;
    }
}

