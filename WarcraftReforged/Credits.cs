﻿using System;

public class Credits
{
    Image background;
    Image transparency;
    TextButton exit;
    Font font25;
    Font font20;
    static Credits cr;

    private  Credits()
    {
        background = new Image("data/menus/creditsMenu.jpg");
        transparency = new Image("data/misc/black.png");
        exit = new TextButton("Return", 200, 600, 100, 30, 20);
        font25 = new Font("data/fonts/joystix.ttf", 25);
        font20 = new Font("data/fonts/joystix.ttf", 20);
    }

    public static Credits GetInstance()
    {
        if( cr == null)
        {
            cr = new Credits();
        }
        return cr;
    }

    public void Run()
    {
        
        do
        {
            SdlHardware.DrawHiddenImage(background, 0, 0);
            SdlHardware.DrawHiddenImage(transparency, 0, 0);
            exit.Draw();

            SdlHardware.WriteHiddenText(
                "WarcraftRefoged created by Ivan Lazcano Sindin"
                , 250, 50, 255, 255, 255, font25);

            SdlHardware.WriteHiddenText(
                "Creaded with TAO_SDL and C# using mono implementation"
                , 300, 200, 255, 255, 255, font20);
            
            SdlHardware.WriteHiddenText(
                "Developed under GNU/Linux with MonoDevelop"
                , 300, 230, 255, 255, 255, font20);

            SdlHardware.ShowHiddenScreen();

            if((exit.CheckSelected(SdlHardware.GetMouseX(),
                                       SdlHardware.GetMouseY())
                && SdlHardware.MouseClicked(1)) 
               || SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
            {
                return;
            }
            SdlHardware.Pause(50);

        } while (true);
       
    }
}

